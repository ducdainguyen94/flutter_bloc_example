import 'dart:io';

import 'package:base_flutter_intlabs/app.dart';
import 'package:base_flutter_intlabs/database/shared_preference_helper.dart';
import 'package:base_flutter_intlabs/inject_dependency/injection.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'router/auth_service.dart';

//Thêm multi langguage
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

// CONFIG FIREBASE
/// Define a top-level named handler which background/terminated messages will
/// call.
///
/// To verify things are working, check out the native platform logs.
/// phương thức để có thể nhận notification khi app chạy dưới background
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}


// class MyHttpoverrides extends HttpOverrides{
//   @override 
//   HttpClient createHttpClient(SecurityContext? context){
//     return super.createHttpClient(context)
//     ..badCertificateCallback = (X509Certificate cert, String host, int port)=>true;
//   }
// }


void main() async{

  WidgetsFlutterBinding.ensureInitialized();
    await EasyLocalization.ensureInitialized();// multi langguage

  //ENV
  // await dotenv.load(fileName: ".env.develop");
  await dotenv.load(fileName: ".env");
  // CONFIG FIREBASE
  await Firebase.initializeApp();
  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  //init get_it
  await Injection.inject();
  //get jwtToken 
  var authToken = await getIt.get<SharedPreferenceHelper>().authToken;
  await Get.putAsync<AuthService>(() => AuthService().init());
  Get.find<AuthService>().setIsAuth(authToken != null ? true : false);

  //svn
  // HttpOverrides.global=new MyHttpoverrides();

  runApp(
      EasyLocalization(
      supportedLocales: [Locale('vi', 'VN'),Locale('en', 'US') ],
      path: 'assets/translations', // <-- change the path of the translation files 
      fallbackLocale: Locale('vi', 'VN'),
      useOnlyLangCode: true,
      child: MyApp()
    ),

    // MyApp()
    );
}
