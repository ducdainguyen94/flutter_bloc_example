import 'package:base_flutter_intlabs/models/entities/user/user_entity.dart';
import 'package:base_flutter_intlabs/models/enums/load_status.dart';
import 'package:equatable/equatable.dart';

class AppState extends Equatable{
  final UserEntity? user;
  final LoadStatus loadStatus;

  AppState({
    this.user, 
    this.loadStatus = LoadStatus.initial
  });

  AppState copyWith({UserEntity? user, LoadStatus? loadStatus}){
    return AppState(
      user: user ?? this.user,
      loadStatus: loadStatus ?? this.loadStatus
    );
  }

  AppState removeUser(){
    return AppState(
      user: null,
      loadStatus: loadStatus
    );
  }

  @override
  // TODO: implement props
  List<Object?> get props => [];
  
}