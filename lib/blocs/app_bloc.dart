import 'package:base_flutter_intlabs/blocs/app_state.dart';
import 'package:base_flutter_intlabs/models/entities/user/user_entity.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppBloc extends Cubit<AppState>{

  AppBloc() : super(AppState());

  ///Sign Out
  Future signOut() async {
    emit(state.copyWith());
  }

  //save user
  void saveUserAuthen(UserEntity userEntity){
    emit(state.copyWith(user: userEntity));
  }

  /// Get detail user info
  Future<void> getInfoUser() async {
    // try {
    //   emit(state.copyWith(fetchProfileStatus: LoadStatus.loading));

    //   final result = await userRepo.getProfile();

    //   emit(state.copyWith(
    //     fetchProfileStatus: LoadStatus.success,
    //   ));

    //   if (result != null) {
    //     updateProfileAndChangeLanguageApp(result.data ?? UserEntity());
    //   } else {
    //     emit(state.copyWith(fetchProfileStatus: LoadStatus.failure));
    //   }
    // } catch (error) {
    //   logger.e(error);

    //   AppSnackBar.showError(
    //     message: ErrorUtils.networkErrorToMessage(error),
    //   );
    // }
  }
}