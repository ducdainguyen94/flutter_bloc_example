import 'package:flutter/material.dart';

class AppFonts {
  static const String regular = 'SFPro';
  static const String medium = 'SF-Pro-Text-Medium';
  static const String bold = 'SFPro-bold';

  static const String fontSFProDisplayRegular = 'SF-Pro-Display-Regular';
  static const String fontSFProDisplayMedium = 'SF-Pro-Display-Medium';
  static const String fontSFProDisplayBold = 'SF-Pro-Display-Bold';

  static const String fontAvertaRegular = 'AvertaStd-Regular';
  static const String fontAvertaStdBold = 'AvertaStd-Bold';

  // static const String regular = 'AvertaStd';
}
