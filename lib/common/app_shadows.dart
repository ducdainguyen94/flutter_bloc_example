import 'package:flutter/material.dart';

import 'app_colors.dart';

class AppShadow {
  static final boxShadow = [
    const BoxShadow(
      color: AppColors.shadow,
      blurRadius: 3,
      offset: Offset(0, 0),
    ),
  ];

  static final boxShadowBottomBar = [
    BoxShadow(
      color: AppColors.primary.withOpacity(0.21),
      offset: const Offset(0, 5),
      blurRadius: 21,
    ),
  ];

  static final boxShadowChallengeModeCard = [
    BoxShadow(
      color: AppColors.primary.withOpacity(0.3),
      offset: const Offset(-10, 18),
      blurRadius: 36,
    ),
  ];

  static final boxShadowLevelCard = [
    BoxShadow(
      color: AppColors.primary.withOpacity(0.2),
      offset: const Offset(-10, 18),
      blurRadius: 36,
    ),
  ];
}
