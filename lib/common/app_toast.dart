import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AppToast {
  static showToast({title, backgroundColor, textColor}) {
    Fluttertoast.showToast(
        msg: title,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        timeInSecForIosWeb: 1,
        backgroundColor: backgroundColor ?? Colors.lightBlue[100],
        textColor: textColor ?? Colors.black,
        fontSize: 16.0);
  }
}
