import 'dart:ui';

class AppColors {
  ///Common
  static const Color primary = Color(0xFF087599);
  static const Color primaryLight = Color.fromARGB(255, 134, 195, 215);
  static const Color secondary = Color(0xFF2C3B5D);
  static const Color themeBackground = Color.fromRGBO(1, 117, 194, 1.0);

  ///Background
  static const Color background = Color(0xFFFEF7E2);
  static const Color card = Color(0xFFFFD966);
  static const Color secondaryButton = Color(0xFF757B88);
  static const Color colorYellow = Color(0xFFFFE8A1);
  static const Color yellowAccent = Color(0xFFFFE8A1);
  static const Color accentTertiary = Color(0xFFFFECB4);
  static const Color errorTertiary = Color(0xFFFFF2F2);
  static const Color white = Color(0xFFFFFFFF);
  static const Color backgroundSearchInput = Color(0xFFFBFBFB);

  ///Shadow
  static const Color shadow = Color(0x25606060);

  ///Border
  static const Color border = Color(0xFFFF3030);

  ///Divider
  static const Color divider = Color(0xFFF1F1F1);
  static const Color dividerAddressDetail = Color(0xFFDADADA);
  static const Color dividerSecondary = Color(0xFFDADADA);
  static const Color dividerAddressDetailQuestion = Color(0xFFFFE8A1);
  static const Color dividerAddressDetailQuestionSuccess = Color(0xFFECB920);

  ///Text
  static const Color textWhite = Color(0xFFFFFFFF);
  static const Color textBlack = Color(0xFF000000);
  static const Color textBlue = Color(0xFF0000FF);
  static const Color textDisable = Color(0xFF89a3b1);
  static const Color textPopup = Color(0xFFECB920);

  ///TextField
  static const Color textFieldEnabledBorder = Color(0xFFF1F1F1); //#F1F1F1  919191
  static const Color textFieldFocusedBorder = Color(0xFFd74315);
  static const Color textFieldDisabledBorder = Color(0xFFF1F1F1);
  static const Color textFieldCursor = Color(0xFF919191);
  static const Color textFieldErrorBorder = Color(0xFFED2121);

  ///Button
  static const Color buttonBGWhite = Color(0xFFcdd0d5);
  static const Color buttonBGTint = Color(0xFFd74315);
  static const Color buttonBorder = Color(0xFFd74315);
  static const Color buttonYellow = Color(0xFFECB920);
  static const Color buttonGreyscale = Color(0xFF2C3B5D);

  /// Tabs
  static const Color imageBG = Color(0xFF919191);
  static const Color textGray = Color(0xFF58595B);

  ///BottomNavigationBar
  static const Color bottomNavigationBar = Color(0xFF919191);

  ///
  static const Color unSelectedBorderColor = Color(0xFFDADADA);
}
