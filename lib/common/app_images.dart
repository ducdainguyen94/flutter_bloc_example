class AppImages {
  //icons
  static const String icUser = "assets/icons/ic_user.png";

  //images
  static const String imgBgLogin = "assets/images/img_exhibition.jpeg";
  static const String avatar = "assets/images/avatar_profile.png";
  static const String empty = "assets/images/Mar-Business_2.jpg";
  static const String englishLanguage = "assets/images/ic_enlish_language.png";
  static const String vietNamLanguage = "assets/images/ic_vietnam_language.png";
  static const String icCancel = "assets/icons/ic_cancel.png";
}
