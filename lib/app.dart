import 'package:base_flutter_intlabs/blocs/app_bloc.dart';
import 'package:base_flutter_intlabs/common/app_loading.dart';
import 'package:base_flutter_intlabs/notification_service/notification_service.dart';
import 'package:base_flutter_intlabs/router/auth_service.dart';
import 'package:base_flutter_intlabs/router/router.dart';
import 'package:base_flutter_intlabs/router/router_name.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:loader_overlay/loader_overlay.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
final GlobalKey listShiftScreenKey = GlobalKey();
final GlobalKey detailScreenKey = GlobalKey();

class MyApp extends StatefulWidget implements WidgetsBindingObserver{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
  
  @override
  void didChangeAccessibilityFeatures() {
    // TODO: implement didChangeAccessibilityFeatures
  }
  
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    print('STATE=========> $state');
  }
  
  @override
  void didChangeLocales(List<Locale>? locales) {
    // TODO: implement didChangeLocales
  }
  
  @override
  void didChangeMetrics() {
    // TODO: implement didChangeMetrics
  }
  
  @override
  void didChangePlatformBrightness() {
    // TODO: implement didChangePlatformBrightness
  }
  
  @override
  void didChangeTextScaleFactor() {
    // TODO: implement didChangeTextScaleFactor
  }
  
  @override
  void didHaveMemoryPressure() {
    // TODO: implement didHaveMemoryPressure
  }
  
  @override
  Future<bool> didPopRoute() {
    // TODO: implement didPopRoute
    throw UnimplementedError();
  }
  
  @override
  Future<bool> didPushRoute(String route) {
    // TODO: implement didPushRoute
    throw UnimplementedError();
  }
  
  @override
  Future<bool> didPushRouteInformation(RouteInformation routeInformation) {
    // TODO: implement didPushRouteInformation
    throw UnimplementedError();
  }
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    //Start Notification service
    NotificationService.instance.start();
  }

  @override
  Widget build(BuildContext context) {
    print(context.locale);
    return MultiBlocProvider(
        providers: [
          BlocProvider<AppBloc>(create: (context) {
            return AppBloc();
          })
        ],
        child: GlobalLoaderOverlay(
        useDefaultLoading: false,
        overlayWidget: Center(
          child: Container(
            width: 100,
            height: 100,
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Color(0xff2A3D4F)
            ),
            child: AppLoading.spinkit
          ),
        ),
        child: ScreenUtilInit(
          designSize: const Size(414, 896),
          minTextAdapt: true,
          splitScreenMode: true,
          builder: (context, child) {
            return GetMaterialApp(
              debugShowCheckedModeBanner: false,
              // Phần setup easy-localtion -đa ngôn ngữ
              localizationsDelegates: context.localizationDelegates,
              supportedLocales: context.supportedLocales,
              locale: context.locale,

              theme: ThemeData(
                  // fontFamily: Fonts.SFPro
                  // fontFamily: Fonts.regular
                  ),
              // onGenerateRoute: MyRouter.generateRoute,
              navigatorKey: navigatorKey,
              initialRoute: RouteName.loginScreen,
              getPages: MyRouter.getPages,
              builder: (context, child) {
                return MediaQuery(
                  child: child ?? Container(),
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                );
              },
            );
          },
          // child: const HomePage(title: 'First Method'),
        )
      )
  );
  }
}
