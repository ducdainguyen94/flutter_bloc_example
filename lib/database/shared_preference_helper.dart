import 'dart:async';
import 'package:base_flutter_intlabs/database/preferences_key.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {
  String? jwtToken;
  // shared pref instance
  final SharedPreferences _sharedPreference;

  // constructor
  SharedPreferenceHelper(this._sharedPreference, {this.jwtToken});

  // General Methods: ----------------------------------------------------------
  Future<String?> get authToken async {
    return _sharedPreference.getString(PreferencesKey.auth_token);
  }

  Future<bool> saveAuthToken(String authToken) async {
    return _sharedPreference.setString(PreferencesKey.auth_token, authToken);
  }

  Future<bool> removeAuthToken() async {
    return _sharedPreference.remove(PreferencesKey.auth_token);
  }

  // organization_id user login: ----------------------------------------------------------
  Future<int?> get organizationId async {
    return _sharedPreference.getInt(PreferencesKey.organization_id);
  }

  Future<bool> saveOrganizationId(int id) async {
    return _sharedPreference.setInt(PreferencesKey.organization_id, id);
  }

  Future<bool> removeOrganizationId() async {
    return _sharedPreference.remove(PreferencesKey.organization_id);
  }

  // token firebase:---------------------------------------------------
  String? get getTokenFcm {
    return _sharedPreference.getString(PreferencesKey.token_firebase);
  }

  Future<void> saveTokenFcm(String token) {
    return _sharedPreference.setString(PreferencesKey.token_firebase, token);
  }

  // save logIn:---------------------------------------------------
  Future<String?> get getLogIn async {
    return _sharedPreference.getString(PreferencesKey.saveLogIn);
  }

  Future<void> saveLogIn(String account) {
    return _sharedPreference.setString(PreferencesKey.saveLogIn, account);
  }

  Future<bool> removeLogIn() async {
    return _sharedPreference.remove(PreferencesKey.saveLogIn);
  }
}
