class PreferencesKey {
  PreferencesKey._();
  static const String user = "user";
  static const String is_logged_in = "isLoggedIn";
  static const String auth_token = "jwtTotken";
  static const String token_firebase = "tokenFirebase";
  static const String organization_id = "organizationId";
  static const String saveLogIn = "saveLogIn";
  // static const String pass_word = "passWord";
}
