// ignore_for_file: unnecessary_this

import 'package:base_flutter_intlabs/common/app_fonts.dart';
import 'package:base_flutter_intlabs/common/app_text_styles.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/bloc/detail_shift_bloc.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/bloc/detail_shift_state.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/widget/item_student.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/widget/text_field_class_shift.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/bloc/shift_list_bloc.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/bloc/shift_list_state.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/shift.dart';
import 'package:base_flutter_intlabs/widgets/auto_complete_checkin.dart';
import 'package:base_flutter_intlabs/widgets/button.dart';
import 'package:base_flutter_intlabs/widgets/dropdown_custom.dart';
import 'package:base_flutter_intlabs/widgets/widget_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tuple/tuple.dart';

import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:intl/intl.dart';
import 'package:base_flutter_intlabs/common/app_colors.dart';

import '../../widgets/dialog_date.dart';
import '../../widgets/input_choose_date.dart';

class DetailShiftScreen extends StatefulWidget {
  DetailShiftScreen({required Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DetailShiftState();
  }
}

class _DetailShiftState extends State<DetailShiftScreen> {
  final RefreshController _refreshController = RefreshController(initialRefresh: false);


  // on Change Day  -- thay đổi ngày trên lịch
  onChangeDay(String startDay, String endDay) {
    print('---$startDay   ---++++ $endDay');
    context.read<DetailShiftBloc>().onChangeDay(startDay, endDay);
  }

  void _onRefresh() async {
    // monitor network fetch
    print('Ref');
    _refreshController.refreshCompleted();
  }


  void _onLoading() async {
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Shift params = Get.arguments;
    print('PARAMS==========$params');
    print('PARAMS==============>>${params.students}');
    context.read<DetailShiftBloc>().getListStudentOfClassShift(context.read<ShiftListBloc>().state.shiftSelected);

  }

  // on change attendance
  onChangeAttendance(String? value){
    context.read<DetailShiftBloc>().onChangeAttendance(value);
  }

  //on change late
  onChangeLate(String? value){
    context.read<DetailShiftBloc>().onChangeLate(value);
  }

  onTextChange(txt){
    context.read<DetailShiftBloc>().onKeywordChange(txt);
  }

  @override
  Widget build(BuildContext context) {
    
    // TODO: implement build
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: const SystemUiOverlayStyle(
          // For Android.
          // Use [light] for white status bar and [dark] for black status bar.
          statusBarIconBrightness: Brightness.light,
          // For iOS.
          // Use [dark] for white status bar and [light] for black status bar.
          statusBarBrightness: Brightness.light,
        ),
        child: GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: SafeArea(
            child: Scaffold(
              appBar: AppBar(
                elevation: 0.0,
                iconTheme: IconThemeData(
                  color: Colors.black
                ),
                backgroundColor: Colors.white,
                title: BlocBuilder<DetailShiftBloc, DetailShiftState>(
                  builder: ((context, state){
                    print('VO DAY   ${state.title}');
                    return Text(state.title ?? 'Chi tiết', style: TextStyle(color: Colors.black),);
                  })
                ),
                actions: [
                  GestureDetector(
                    onTap: (){
                      context.read<DetailShiftBloc>().showViewFilter();
                    },
                    child: Container(
                      padding: EdgeInsets.only(left: 10.w, right: 10.w),
                      child: Icon(Icons.filter_alt)
                    )
                  )
                ],
              ),
              body: Container(
                width: double.infinity,
                height: double.infinity,
                child: Column(
                children: [   
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(
                      bottom: 10.h,
                    ),
                    padding: EdgeInsets.only(left: 16.w, right: 16.w),
                    child: WidgetSearch(title: 'Tìm kiếm', onTextChange: onTextChange,),
                  ), 
                  
                     
                  BlocSelector<DetailShiftBloc, DetailShiftState, Tuple3<bool, String?, String?>>(
                  selector: ((state) => Tuple3<bool, String?, String?>(state.showFilter, state.startDay, state.endDay)),
                  builder:(context, state)=> state.item1 ? Container(
                      width: double.infinity,
                      // height: 200.h,
                      padding: EdgeInsets.symmetric(horizontal: 16.w),
                      child: Column(
                        children: [   
                          BlocSelector<DetailShiftBloc, DetailShiftState, Tuple4<List<String>, List<String>, String, String>>(
                            selector: ((state) => Tuple4(state.attendance, state.late, state.attendanceSelected, state.lateSelected)),
                            builder:(context, state) => Row(children: [
                              Expanded(child: DropDownCustom(items: state.item1, selectedValue: state.item3, onChange: onChangeAttendance,)), 
                              SizedBox(width: 20,),
                              Expanded(child: DropDownCustom(items: state.item2, selectedValue: state.item4, onChange: onChangeLate,)),
                            ],),
                          ),                     
                          
                        ],
                      ),
                    ) : Container(),
                  ),
                  // Container(
                  //           margin: EdgeInsets.only(top: 10,left: 10,right: 10),
                  //           height: 45.h,
                  //           padding: EdgeInsets.only(left: 10.w, right: 10.w),
                  //           decoration: BoxDecoration(
                  //             color: Colors.blue,
                  //             borderRadius: BorderRadius.all(Radius.circular(8))
                  //           ),
                  //           child: InkWell(
                  //             onTap: () {
                  //                 context.read<DetailShiftBloc>().actionApdung();
                                  
                  //             },
                  //             child: Center(child: Text('Áp dụng', style: TextStyle(color: Colors.white),))),
                  //         ),
                  // SizedBox(
                  //   height: 10.h,
                  // ),

                  Padding(
                        padding: EdgeInsets.only(left: 16.w, top: 10.h),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: BlocBuilder<ShiftListBloc, ShiftListState>(
                            builder:(context, state) =>  RichText(
                              text: TextSpan(
                                text: 'Số học sinh điểm danh: ',
                                style: TextStyle(fontFamily: AppFonts.bold, fontSize: 18.sp, color: Colors.black),
                                children: <TextSpan>[
                                  TextSpan(text: '${context.read<ShiftListBloc>().state.shiftSelected?.attendance?.registered!}', style: AppTextStyle.blackS16Bold.copyWith(fontSize: 18.sp, color: AppColors.primary)),
                                  TextSpan(text: '/${context.read<ShiftListBloc>().state.shiftSelected?.attendance?.totalStudent!}', style: AppTextStyle.blackS16Bold.copyWith(fontSize: 18.sp)),
                                ],
                              ),
                            ),
                          )
                          // child: Text('Danh sách học sinh', style: TextStyle(fontFamily: AppFonts.bold, fontSize: 16.sp),)
                        ),
                      ),

                  Expanded(
                    child: BlocBuilder<DetailShiftBloc, DetailShiftState>(
                      builder: (context, state) => state.listStudent != null && state.listStudent!.length > 0 ? Container(
                          padding: EdgeInsets.only(top: 5.h),
                          child: SmartRefresher(
                          enablePullDown: true,
                          enablePullUp: false,
                          header: WaterDropHeader(),
                          controller: _refreshController,
                          onRefresh: _onRefresh,
                          onLoading: _onLoading,
                          child: ListView.builder(
                            padding: EdgeInsets.only(bottom: 20),
                            // shrinkWrap: true,
                            itemCount: state.listStudent?.length,
                            itemBuilder: (context, position) => ItemStudent(item: state.listStudent![position], index: position,),
                          ),
                        )
                      )
                      : Container(),
                    ),
                  ),
                ],
              )),
            ),
          ),
    ));
  }
}
