import 'dart:math';

import 'package:base_flutter_intlabs/app.dart';
import 'package:base_flutter_intlabs/inject_dependency/injection.dart';
import 'package:base_flutter_intlabs/models/response/base_api_response.dart';
import 'package:base_flutter_intlabs/network/api.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/bloc/shift_list_bloc.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/shift.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/students.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:intl/intl.dart';
import 'package:loader_overlay/loader_overlay.dart';

import 'detail_shift_state.dart';

class DetailShiftBloc extends Cubit<DetailShiftState> {
  DetailShiftBloc()
      : super(DetailShiftState(
            keyword: '',
            startDay: DateFormat('yyyy-MM-dd').format(DateTime.now().subtract(const Duration(days: 6))),
            endDay: DateFormat('yyyy-MM-dd').format(DateTime.now()),
            showFilter: true,
            isShowDialog: false,
            classShift: 'adsfjkljf',
            attendance: ['Tất cả', 'Đã điểm danh', 'Chưa điểm danh'],
            late: ['Tất cả', 'Đi muộn'],
            attendanceSelected: 'Tất cả',
            lateSelected: 'Tất cả',
            title: 'Chi tiết'
          ));

  // on change Day
  void onChangeDay(String startDay, String endDay) {
    emit(state.copyWith(startDay: startDay, endDay: endDay));
  }

  String removeDiacritics(String str) {
    var withDia = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var withoutDia = 'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';

    for (int i = 0; i < withDia.length; i++) {
      str = str.replaceAll(withDia[i], withoutDia[i]);
    }
    return str.toUpperCase();
  }

  //click button apply filter
  void onKeywordChange(String txt) {

    //Filter data
    List<Students> listFilterSearch = [];
    state.listStudentSource?.forEach((e) {
      if (removeDiacritics(e.nameStudent ?? '').contains(txt.toUpperCase())) {
        listFilterSearch.add(e);
      }
    });

    print('CKCKKCKCKyyyK============ ${listFilterSearch.length}');

    List<Students> listFinallyFilter = [];
    if(state.attendanceSelected == 'Tất cả' || state.attendanceSelected == 'Đã điểm danh'){
        if(state.attendanceSelected == 'Tất cả' && state.lateSelected == 'Tất cả'){
          listFinallyFilter = listFilterSearch;
        }else if((state.attendanceSelected == 'Tất cả' || state.attendanceSelected == 'Đã điểm danh') && state.lateSelected == 'Đi muộn'){
          listFilterSearch.forEach((e) {
              if(e.timeCheckin != null){
                DateTime timeStart = DateTime.parse(e.startDate ?? '');
                DateTime timeLate = timeStart.add(Duration(minutes: 3));
                DateTime timeCheckinStudent = DateTime.parse(e.timeCheckin ?? '');
                if(timeLate.compareTo(timeCheckinStudent) < 0){
                  listFinallyFilter.add(e);
                }
              }
          });
        }else if(state.attendanceSelected == 'Đã điểm danh' && state.lateSelected == 'Tất cả'){
          listFilterSearch.forEach((e) {
              if(e.timeCheckin != null){            
                  listFinallyFilter.add(e);
              }
          });
        }
    }else {
      if(state.lateSelected == 'Đi muộn'){
        listFinallyFilter = [];
      }else {
        listFinallyFilter = listFilterSearch.where((element) => element.checkin == false).toList();
      }    
    }
    emit(state.copyWith(keyword: txt, listStudent: listFinallyFilter));
  }

  //onchange attendance
  void onChangeAttendance(String? value) {
    //Filter data
    List<Students> listFilterSearch = [];
    state.listStudentSource?.forEach((e) {
      if (removeDiacritics(e.nameStudent ?? '').contains(state.keyword.toUpperCase())) {
        listFilterSearch.add(e);
      }
    });

    print('CKCKKCKCKyyyK============onChangeAttendance ${listFilterSearch.length}');

    List<Students> listFinallyFilter = [];
    if(value == 'Tất cả' || value == 'Đã điểm danh'){
        if(value == 'Tất cả' && state.lateSelected == 'Tất cả'){
          listFinallyFilter = listFilterSearch;
        }else if((value == 'Tất cả' || value == 'Đã điểm danh') && state.lateSelected == 'Đi muộn'){
          listFilterSearch.forEach((e) {
              if(e.timeCheckin != null){
                DateTime timeStart = DateTime.parse(e.startDate ?? '');
                DateTime timeLate = timeStart.add(Duration(minutes: 3));
                DateTime timeCheckinStudent = DateTime.parse(e.timeCheckin ?? '');
                if(timeLate.compareTo(timeCheckinStudent) < 0){
                  listFinallyFilter.add(e);
                }
              }
          });
        }else if(value == 'Đã điểm danh' && state.lateSelected == 'Tất cả'){
          listFilterSearch.forEach((e) {
              if(e.timeCheckin != null){            
                  listFinallyFilter.add(e);
              }
          });
        }
    }else {
      if(state.lateSelected == 'Đi muộn'){
        listFinallyFilter = [];
      }else {
        listFinallyFilter = listFilterSearch.where((element) => element.checkin == false).toList();
      }  
    }

    emit(state.copyWith(attendanceSelected: value, listStudent: listFinallyFilter));

  }

  //onchange late
  void onChangeLate(String? value){
    //Filter data
    List<Students> listFilterSearch = [];
    state.listStudentSource?.forEach((e) {
      if (removeDiacritics(e.nameStudent ?? '').contains(state.keyword.toUpperCase())) {
        listFilterSearch.add(e);
      }
    });

    List<Students> listFinallyFilter = [];
    if(state.attendanceSelected == 'Tất cả' || state.attendanceSelected == 'Đã điểm danh'){
        if(state.attendanceSelected == 'Tất cả' && value == 'Tất cả'){
          listFinallyFilter = listFilterSearch;
        }else if((state.attendanceSelected == 'Tất cả' || state.attendanceSelected == 'Đã điểm danh') && value == 'Đi muộn'){
          listFilterSearch.forEach((e) {
              if(e.timeCheckin != null){
                DateTime timeStart = DateTime.parse(e.startDate ?? '');
                DateTime timeLate = timeStart.add(Duration(minutes: 3));
                DateTime timeCheckinStudent = DateTime.parse(e.timeCheckin ?? '');
                if(timeLate.compareTo(timeCheckinStudent) < 0){
                  listFinallyFilter.add(e);
                }
              }
          });
        }else if(state.attendanceSelected == 'Đã điểm danh' && value == 'Tất cả'){
          listFilterSearch.forEach((e) {
              if(e.timeCheckin != null){            
                  listFinallyFilter.add(e);
              }
          });
        }
    }else {
      if(value == 'Đi muộn'){
        listFinallyFilter = [];
      }else {
        listFinallyFilter = listFilterSearch.where((element) => element.checkin == false).toList();
      }  
    }

    emit(state.copyWith(lateSelected: value, listStudent: listFinallyFilter));

  }

  getListClassShift() {
    emit(state.copyWith(listClass: ["Lớp a", "Lớp B", "Lớp C"]));
  }

  //get shift list
  getDetailShiftList() {
    emit(state.copyWith(listStudent: []));
  }

  //show view filter
  void showViewFilter() {
    emit(state.copyWith(showFilter: !state.showFilter));
  }


  void actionApdung(){
    List<Students> newList = [];
    state.listStudentSource?.forEach((e){
      String txt = e.checkin == true ? "Đã điểm danh" : "Chưa điểm danh";
      if(state.attendanceSelected == "Tất cả" && state.lateSelected == "Tất cả"){
        newList.clear();
        if((e.nameStudent ?? '').toUpperCase().contains(state.keyword.toUpperCase())){
          newList.add(e);
        }
      }
      if(state.attendanceSelected == "Tất cả" && (e.nameStudent ?? '').toUpperCase().contains(state.keyword.toUpperCase())){
        if(state.lateSelected == "Tất cả"){
          newList.add(e);
        } else{
         if(e.timeCheckin != null){
          DateTime timeStart = DateTime.parse(e.startDate ?? '');
          DateTime timeLate = timeStart.add(Duration(minutes: 3));
          DateTime timeCheckinStudent = DateTime.parse(e.timeCheckin ?? '');
           if(timeLate.compareTo(timeCheckinStudent) < 0){
              newList.add(e);
            }
          }
        }
      }
      if(state.attendanceSelected != "Tất cả" && state.attendanceSelected == txt && (e.nameStudent ?? '').toUpperCase().contains(state.keyword.toUpperCase())){
        if(state.lateSelected == "Tất cả"){
          newList.add(e);
        } else{
         if(e.timeCheckin != null){
          DateTime timeStart = DateTime.parse(e.startDate ?? '');
          DateTime timeLate = timeStart.add(Duration(minutes: 3));
          DateTime timeCheckinStudent = DateTime.parse(e.timeCheckin ?? '');
           if(timeLate.compareTo(timeCheckinStudent) < 0){
              newList.add(e);
            }
          }
        }
      }
    });
    emit(state.copyWith(listStudent: newList));
  }

  // void actionApdung(){
  //   List<Students> newList = [];
  //   state.listStudentSource?.forEach((e){
  //     String txt = e.checkin == true ? "Đã điểm danh" : "Chưa điểm danh";
  //     if(state.attendanceSelected == "Tất cả"){
  //       if((e.nameStudent ?? '').toUpperCase().contains(state.keyword.toUpperCase())){
  //         newList.add(e);
  //       }
  //     }
  //     if(state.attendanceSelected == txt && (e.nameStudent ?? '').toUpperCase().contains(state.keyword.toUpperCase())){
  //        newList.add(e);
  //     }
  //   });
  //   emit(state.copyWith(listStudent: newList));
  // }

   //init data
  void getListStudentOfClassShift(Shift? data) async {
    if(data != null && data.students != null){
        for(int i = 0; i < data.students!.length ; i++){
          data.students![i].startDate = data.startDate;
          data.students![i].endDate = data.endDate;
        }
        List<Students> listTemporary =  data.students!.map((e) => e.copyWith()).toList();
        emit(state.copyWith(listStudentSource: listTemporary, listStudent: data.students, title: data.className));
    }else {
      emit(state.copyWith(title: data?.className));
    }  
  }

  // Điểm danh cho 1 học sinh
  void Attendance({
    int? workDayId,
  }) async {
    try {
      // Map<String, dynamic> params = {
      //   "classId": classId,
      //   "scheduleId": scheduleId,
      //   "studentId": studentId,
      // };

      print('Điểm danh cho 1 học sinh $workDayId');

      var res = await getIt.get<Api>().actionAttendance(workDayId!);
      final result = BaseApiResponse.fromJson(res); // Chỉ làm mục đích đưa chuỗi Json --> List
      print(' ATTENDANCE ________________result.toJson():  ${result.toJson()}'); // Cái này chỉ để in ra để nhìn thôi
      print(' ATTENDANCE ________________success :  ${result.success}');
      print(' ATTENDANCE ________________data.toString() : ${result.data.toString()}');

      if (result.success) {
        print('ATTENDANCE RESULT SUCCESS______ ');
        // List<Students> dataStudent = (result.data as List).map((e) => Students.fromJson(e)).toList();
        // emit(state.copyWith(listStudent: dataStudent));
        // print('RESULT SUCCESS______ done ${dataStudent[0].toJson()}');

        listShiftScreenKey.currentContext!.read<ShiftListBloc>().getListShiftOfTeacher();
        
      } else {
        print('ATTENDANCE ERROR=> ');
      }
      navigatorKey.currentContext!.loaderOverlay.hide();
    } catch (e) {
      navigatorKey.currentContext!.loaderOverlay.hide();
      print('ATTENDANCE error--- $e');
    }
  }
}
