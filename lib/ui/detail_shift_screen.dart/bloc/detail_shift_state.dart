import 'package:base_flutter_intlabs/models/entities/event/event.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/students.dart';

class DetailShiftState {
  String keyword;
  String? startDay;
  String? endDay;
  List<Students>? listStudentSource;
  List<Students>? listStudent;
  List<Students>? listStudentCache;
  List<String>? listClass;
  String? classShift;
  bool showFilter = false;
  bool isShowDialog;
  List<String> attendance;
  List<String> late;
  String attendanceSelected;
  String lateSelected;
  String title;

  DetailShiftState({
    required this.keyword,
    required this.isShowDialog,
    this.startDay,
    this.endDay,
    this.listStudentSource,
    this.listStudent,
    this.listStudentCache,
    required this.showFilter,
    List<String>? listClass,
    String? classShift,
    required this.attendance,
    required this.late,
    required this.attendanceSelected,
    required this.lateSelected,
    required this.title
  });

  DetailShiftState copyWith({
    String? keyword,
    bool? isShowDialog,
    String? endDay,
    String? startDay,
    List<Students>? listStudentSource,
    List<Students>? listStudent,
    List<Students>? listStudentCache,
    bool? showFilter,
    String? classShift,
    List<String>? listClass,
    List<String>? attendance,
    List<String>? late,
    String? attendanceSelected,
    String? lateSelected,
    String? title,
  }) =>
      DetailShiftState(
        keyword: keyword ?? this.keyword,
        isShowDialog: isShowDialog ?? this.isShowDialog,
        startDay: startDay ?? this.startDay,
        endDay: endDay ?? this.endDay,
        listStudentSource: listStudentSource ?? this.listStudentSource,
        listStudent: listStudent ?? this.listStudent,
        listStudentCache: listStudentCache ?? this.listStudentCache,
        showFilter: showFilter ?? this.showFilter,
        listClass: listClass ?? this.listClass,
        classShift: classShift ?? this.classShift,
        attendance: attendance ?? this.attendance,
        late: late ?? this.late,
        attendanceSelected: attendanceSelected ?? this.attendanceSelected,
        lateSelected: lateSelected ?? this.lateSelected,
        title: title ?? this.title
      );
}
