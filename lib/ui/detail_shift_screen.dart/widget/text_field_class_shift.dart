import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class TextFieldClassShift extends StatefulWidget {
  List<String>? listClass;
  List<String> listTest =[ 'Lớp a', 'Lớp B', 'Lớp C', 'Lớp D'];

  TextFieldClassShift({Key? key, this.listClass}) : super(key: key);

  @override
  State<TextFieldClassShift> createState() => _TextFieldClassShiftState();
}

class _TextFieldClassShiftState extends State<TextFieldClassShift> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("listClass -------- ${widget.listClass?.length}");
    return (Container(
      child:  Autocomplete<String>(
      optionsBuilder: (TextEditingValue textEditingValue) {
        if (textEditingValue.text == '') {
          return const Iterable<String>.empty();
        }
        return widget.listTest.where((String option) {
          return option.contains(textEditingValue.text.toLowerCase());
        });
      },
      onSelected: (String selection) {
        debugPrint('You just selected $selection');
      },
    ),
    ));
  }
}
