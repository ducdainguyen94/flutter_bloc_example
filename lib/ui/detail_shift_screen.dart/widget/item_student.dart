import 'package:base_flutter_intlabs/app.dart';
import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:base_flutter_intlabs/common/app_fonts.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/bloc/detail_shift_bloc.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/students.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loader_overlay/loader_overlay.dart';

import '../../../widgets/button_custom.dart';

class ItemStudent extends StatefulWidget {
  Students item;
  int index;
  ItemStudent({Key? key, required this.item, required this.index}) : super(key: key);
  @override
  State<ItemStudent> createState() => _ItemStudentState();
}

class _ItemStudentState extends State<ItemStudent> {
  // on Change Day
  String convertTime(int? time) {
    print('--- start $time   ');

    if (time != null) {
      int a = time ~/ 60;
      int b = time % 60;
      print(" convert time $b ");

      return b == 0 ? '${a}:${b}0' : '${a}:${b}';
    } else {
      return '00:00';
    }
  }

  String convertDateString(String str) {
    return DateFormat('HH:mm').format(DateTime.parse(str).add(Duration(hours: 7)));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("student ${widget.item.toJson()}");
    // print('VO DREND RE');
    return Container(
      padding: EdgeInsets.all(10.h),
      margin: EdgeInsets.only(bottom: 10.h, left: 16.w, right: 16.w, top: widget.index == 0 ? 10.h : 0),
      decoration: BoxDecoration(
        color: widget.item.checkin != null && widget.item.checkin == true ?  Colors.blue[50] : Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0.0, 1.0), // changes position of shadow
          ),
        ],
      ),
      child: Row(children: [
        Expanded(
          child: Column(
            children: [
              Column(children: [
                // Container(
                //   padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 20.w),
                //   child: Row(
                //     children: [
                //       Expanded(flex: 1, child: Text('Mã học sinh: ')),
                //       SizedBox(
                //         width: 50,
                //       ),
                //       Expanded(
                //           flex: 2,
                //           child: Text(
                //             '${widget.item.studentCode ?? ''}',
                //             textAlign: TextAlign.left,
                //             style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),
                //           )),
                //     ],
                //   ),
                // ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 20.w),
                  child: Row(
                    children: [
                      Expanded(flex: 1, child: Text('Họ và Tên: ')),
                      SizedBox(
                        width: 50,
                      ),
                      Expanded(
                          flex: 2,
                          child: Text(
                            '${widget.item.nameStudent ?? ''}',
                            textAlign: TextAlign.left,
                            style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),
                          )),
                    ],
                  ),
                ),
                // Container(
                //   padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 20.w),
                //   child: Row(
                //     children: [
                //       Expanded(flex: 1, child: Text('Lớp: ')),
                //       SizedBox(
                //         width: 50,
                //       ),
                //       Expanded(
                //           flex: 2,
                //           child: Text(
                //             '${widget.item.className ?? ''}',
                //             textAlign: TextAlign.left,
                //             style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),
                //           )),
                //     ],
                //   ),
                // ),

                // Container(
                //   padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 20.w),
                //   child: Row(
                //     children: [
                //       Expanded(flex: 1, child: Text('Ca học: ')),
                //       SizedBox(
                //         width: 50,
                //       ),
                //       Expanded(
                //           flex: 2,
                //           child: Text(
                //             '${widget.item.scheduleName ?? "" }: ${convertDateString(widget.item.startDate ?? "")}- ${convertDateString(widget.item.endDate ?? "")}',
                //             textAlign: TextAlign.left,
                //             style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),
                //           )),
                //     ],
                //   ),
                // ),

                // Container(
                //   padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 20.w),
                //   child: Row(
                //     children: [
                //       Expanded(flex: 1, child: Text('Ngày: ')),
                //       SizedBox(
                //         width: 50,
                //       ),
                //       Expanded(
                //           flex: 2,
                //           child: Text(
                //             '${DateFormat('dd/MM/yyyy').format(DateTime.parse(widget.item.startDate!))}',
                //             textAlign: TextAlign.left,
                //             style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),
                //           )),
                //     ],
                //   ),
                // ),

                Container(
                  padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 20.w),
                  child: Row(
                    children: [
                      Expanded(flex: 1, child: Text('Giờ ghi nhận: ')),
                      SizedBox(
                        width: 50,
                      ),
                      Expanded(
                          flex: 2,
                          child: Text(
                            widget.item.timeCheckin != null
                                ? '${convertDateString(widget.item.timeCheckin ?? "") }'
                                : widget.item.timeCheckout != null
                                    ? '${convertDateString(widget.item.timeCheckout ?? "" )  }'
                                    : '',
                            textAlign: TextAlign.left,
                            style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),
                          )),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 20.w),
                  child: Row(
                    children: [
                      Expanded(flex: 1, child: Text('Trạng thái: ')),
                      SizedBox(
                        width: 50,
                      ),
                      Expanded(
                          flex: 2,
                          child: Text(
                            widget.item.checkin == false ? 'Chưa điểm danh' : 'Đã điểm danh',
                            textAlign: TextAlign.left,
                            style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),
                          )),
                    ],
                  ),
                ),


                Container(
                  padding: EdgeInsets.only(right: 10.w,top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      widget.item.timeCheckin == null && widget.item.timeCheckout == null
                      // widget.item.checkin == false
                          ? 
                          ButtonCustom(
                              width: 150.w,
                              backgroundColor: AppColors.primary,
                              title: 'Điểm danh',
                              loading: false,
                              onClick: () {
                                print("ATTENDANCE---------");
                                navigatorKey.currentContext!.loaderOverlay.show();
                                context.read<DetailShiftBloc>().Attendance(workDayId: widget.item.workDayId );
                              },
                            )
                          : Container(),
                          
                      // SizedBox(height: 30.h),
                      // widget.item.timeCheckin == null && widget.item.timeCheckout == null
                      //     ? ButtonCustom(
                      //         width: 150.w,
                      //         backgroundColor: Colors.black54,
                      //         title: 'Xoá',
                      //         loading: false,
                      //         onClick: () {
                      //           print("XOÁ-------");
                      //         },
                      //       )
                      //     : Container(),
                    ],
                  ),
                )



              ])
            ],
          ),
        ),
      ]),
    );
  }

  Container _buttonDiemDanh(BuildContext context) {
    return Container(
                padding: EdgeInsets.only(right: 10.w,top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    widget.item.timeCheckin == null && widget.item.timeCheckout == null
                    // widget.item.checkin == false
                        ? 
                        ButtonCustom(
                            width: 100.w,
                            backgroundColor: AppColors.primary,
                            title: 'Điểm danh',
                            loading: false,
                            onClick: () {
                              print("ATTENDANCE---------");
                              context.read<DetailShiftBloc>().Attendance(workDayId: widget.item.workDayId );
                            },
                          )
                        : Container(),
                        
                    // SizedBox(height: 30.h),
                    // widget.item.timeCheckin == null && widget.item.timeCheckout == null
                    //     ? ButtonCustom(
                    //         width: 150.w,
                    //         backgroundColor: Colors.black54,
                    //         title: 'Xoá',
                    //         loading: false,
                    //         onClick: () {
                    //           print("XOÁ-------");
                    //         },
                    //       )
                    //     : Container(),
                  ],
                ),
              );
  }
}
