import 'dart:ui';

import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:base_flutter_intlabs/common/app_fonts.dart';
import 'package:base_flutter_intlabs/router/router_name.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/bloc/shift_list_bloc.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/bloc/shift_list_state.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/shift.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/widgets/item_shift.dart';
import 'package:base_flutter_intlabs/widgets/button_custom.dart';
import 'package:base_flutter_intlabs/widgets/dropdown_custom.dart';
import 'package:base_flutter_intlabs/widgets/header_app.dart';
import 'package:base_flutter_intlabs/widgets/input_choose_date.dart';
import 'package:base_flutter_intlabs/widgets/nav_drawer.dart';
import 'package:base_flutter_intlabs/widgets/widget_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tuple/tuple.dart';

class ShiftListScreen extends StatefulWidget {
  ShiftListScreen({required Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ShiftListState();
  }
}

class _ShiftListState extends State<ShiftListScreen> {
  RefreshController _refreshController = RefreshController(initialRefresh: true);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.read<ShiftListBloc>().getListShiftOfTeacher();
    context.read<ShiftListBloc>().getListClassOfTeacher();
  }

  void _onRefresh() async {
    // monitor network fetch
    print('Ref');
    context.read<ShiftListBloc>().getListShiftOfTeacher();
    
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    _refreshController.loadComplete();
  }

  void _onTextChange(value) {
    // context.read<ShiftListBloc>().getListShiftOfTeacher();
    context.read<ShiftListBloc>().onChangeStringSearch(value);
    print('Ref $value');
  }

  // on change Class
  onChangeClass(String? value) {
    print("onChangeClass $value");
    context.read<ShiftListBloc>().onChangeClass(value);
  }

// on change day
  onChangeDay(String? startDay, String? endDay) {
    context.read<ShiftListBloc>().onChangeDay(startDay, endDay);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: const SystemUiOverlayStyle(
          // For Android.
          // Use [light] for white status bar and [dark] for black status bar.
          statusBarIconBrightness: Brightness.light,
          // For iOS.
          // Use [dark] for white status bar and [light] for black status bar.
          statusBarBrightness: Brightness.light,
        ),
        child: GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: SafeArea(
            child: Scaffold(
              drawer: NavDrawer(),
              body: Container(
                  width: double.infinity,
                  height: double.infinity,
                  child: Column(
                    children: [
                      BlocBuilder<ShiftListBloc, ShiftListState>(
                        builder: (context, state) => HeaderApp(title: state.listShift != null && state.listShift.length > 0 ? state.listShift[0].nameTeacher! : ''),
                      ),

                      SizedBox(
                        height: 20.h,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(
                          bottom: 0.h,
                          right: 16.w,
                          left: 16.w,
                        ),
                        child: WidgetSearch(
                          title: 'Tìm kiếm',
                          onTextChange: _onTextChange,
                        ),
                      ),

                      BlocSelector<ShiftListBloc, ShiftListState, Tuple3<bool, String?, String?>>(
                        selector: ((state) => Tuple3<bool, String?, String?>(state.showFilter, state.startDay, state.endDay)),
                        builder: (context, state) => state.item1
                            ? Container(
                                width: double.infinity,
                                // height: 200.h,
                                padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 15.h),
                                child: Column(
                                  children: [
                                    BlocSelector<ShiftListBloc, ShiftListState, Tuple4<List<String>, String?, String?, String?>>(
                                      selector: ((state) => Tuple4(state.listClass, state.classSelected, state.startDay, state.endDay)),
                                      builder: (context, state) => Row(
                                        children: [
                                          Expanded(
                                              child: DropDownCustom(
                                            items: state.item1,
                                            selectedValue: state.item2,
                                            onChange: onChangeClass,
                                          )),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Expanded(
                                              child: Container(
                                            child: InputChooseDate(startTime: state.item3, endTime: state.item4, onChangeDay: onChangeDay),
                                          ))
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      height: 45.h,
                                      padding: EdgeInsets.only(left: 10.w, right: 10.w),
                                      decoration: BoxDecoration(color: AppColors.primary, borderRadius: BorderRadius.all(Radius.circular(8))),
                                      child: InkWell(
                                          onTap: () {
                                            // context.read<ShiftListBloc>().getListShiftOfTeacher();
                                           _refreshController.requestRefresh();
                                          },
                                          child: Center(
                                              child: Text(
                                            'Áp dụng',
                                            style: TextStyle(color: Colors.white),
                                          ))),
                                    )
                                  ],
                                ),
                              )
                            : Container(),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 16.w, top: 10.h),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text('Danh sách ca', style: TextStyle(fontFamily: AppFonts.bold, fontSize: 16.sp),)),
                      ),

                      Expanded(
                        child: BlocSelector<ShiftListBloc, ShiftListState, List<Shift>>(
                          selector: ((state) => state.listShift),
                          builder: (context, state) => Container(
                              padding: EdgeInsets.only(top: 0.h),
                              child: SmartRefresher(
                                enablePullDown: true,
                                enablePullUp: false,
                                header: WaterDropHeader(),
                                controller: _refreshController,
                                onRefresh: _onRefresh,
                                onLoading: _onLoading,
                                child: ListView.builder(
                                  padding: EdgeInsets.only(bottom: 20),
                                  // shrinkWrap: true,
                                  itemCount: state.length,
                                  itemBuilder: (context, position) => ItemShift(item: state[position], index: position),
                                ),
                              )),
                        ),
                      )
                    ],
                  )),
            ),
          ),
        ));
  }
}