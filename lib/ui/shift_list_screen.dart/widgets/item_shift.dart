import 'package:base_flutter_intlabs/common/app_fonts.dart';
import 'package:base_flutter_intlabs/router/router_name.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/bloc/shift_list_bloc.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/shift.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ItemShift extends StatelessWidget {
  Shift item;
  int index;
  ItemShift({required this.item, required this.index});

  @override
  Widget build(BuildContext context) {
    print('ITEMMMMMMM ==== ${item.startDate}');
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        context.read<ShiftListBloc>().setItemSelected(item);
        Get.toNamed(RouteName.detailShiftScreen, arguments: item)?.whenComplete(() => {
          context.read<ShiftListBloc>().setItemSelected(null)
        });
      },
      child: Container(
        // height: 100.h,
        padding: EdgeInsets.all(10.h),
        margin: EdgeInsets.only(bottom: 10.h, left: 16.w, right: 16.w, top: index == 0 ? 10.h : 0),
        decoration: BoxDecoration(
          color: Colors.blue[50],
          borderRadius: BorderRadius.all(Radius.circular(8)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(0.0, 1.0), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

               Row(
              children: [
                Expanded(flex: 1, child: Text('Ngày dạy: ')),
                Container(
                  width: 20.w,
                ),
                Expanded(flex: 2, child: Text('${DateFormat('dd/MM/yyy').format(DateTime.parse(item.startDate!))}', 
                style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),)),
              ],
            ),
                   SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Expanded(flex: 1, child: Text('Tên ca: ')),
                Container(
                  width: 20.w,
                ),
                Expanded(flex: 2, child: Text('${item.className}', 
                style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),)),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Expanded(flex: 1, child: Text('Thời gian: ')),
                Container(
                  width: 20.w,
                ),
                Expanded(
                    flex: 2,
                    child:
                        Text('${DateFormat('HH:mm').format(DateTime.parse(item.startDate!).add(Duration(hours: 7)))} - ${DateFormat('HH:mm').format(DateTime.parse(item.endDate!).add(Duration(hours: 7)))}', 
                        style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),)),
              ],
            ),
            // SizedBox(
            //   height: 5,
            // ),
            // Row(
            //   children: [
            //     Expanded(flex: 1, child: Text('Giáo viên: ')),
            //     Container(
            //       width: 20.w,
            //     ),
            //     Expanded(flex: 2, child: Text('${item.nameTeacher}', style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),)),
            //   ],
            // ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Expanded(flex: 1, child: Text('Điểm danh: ')),
                Container(
                  width: 20.w,
                ),
                Expanded(flex: 2, child: Text('${item.attendance!.registered!} / ${item.attendance?.totalStudent!}', 
                style: TextStyle(color: Colors.black, fontFamily: AppFonts.bold),)),
              ],
            )
          ],
        ),
      ),
    );
  }
}
