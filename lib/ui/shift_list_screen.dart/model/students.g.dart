// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'students.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Students _$StudentsFromJson(Map<String, dynamic> json) => Students(
      timeCheckin: json['timeCheckin'] as String?,
      timeCheckout: json['timeCheckout'] as String?,
      nameStudent: json['nameStudent'] as String?,
      className: json['className'] as String?,
      classId: json['classId'] as int?,
      studentCode: json['studentCode'] as String?,
      checkin: json['checkin'] as bool?,
      startDate: json['startDate'] as String?,
      endDate: json['endDate'] as String?,
      scheduleId: json['scheduleId'] as int?,
      workDayId: json['workDayId'] as int?,
    )
      ..scheduleName = json['scheduleName'] as String?
      ..studentId = json['studentId'] as int?;

Map<String, dynamic> _$StudentsToJson(Students instance) => <String, dynamic>{
      'timeCheckin': instance.timeCheckin,
      'timeCheckout': instance.timeCheckout,
      'nameStudent': instance.nameStudent,
      'className': instance.className,
      'classId': instance.classId,
      'scheduleName': instance.scheduleName,
      'studentId': instance.studentId,
      'studentCode': instance.studentCode,
      'checkin': instance.checkin,
      'scheduleId': instance.scheduleId,
      'workDayId': instance.workDayId,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
    };
