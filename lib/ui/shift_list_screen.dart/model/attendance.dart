import 'package:json_annotation/json_annotation.dart';

part 'attendance.g.dart';
@JsonSerializable()
class Attendance {
  int? registered;
  int? totalStudent;
 
  Attendance({ this.registered,  this.totalStudent,});

  factory Attendance.fromJson(Map<String, dynamic> json) => _$AttendanceFromJson(json);

  Map<String, dynamic> toJson() => _$AttendanceToJson(this);

}
