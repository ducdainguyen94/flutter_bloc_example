import 'package:json_annotation/json_annotation.dart';

part 'students.g.dart';

@JsonSerializable()
class Students {
  String? timeCheckin;
  String? timeCheckout;
  String? nameStudent;
  String? className;
  int? classId;
  String? scheduleName;
  int? studentId;
  String? studentCode;
  bool? checkin;
  int? scheduleId;
  int? workDayId;
  //add
  String? startDate;
  String? endDate;

  Students(
      {this.timeCheckin,
      this.timeCheckout,
      this.nameStudent,
      this.className,
      this.classId,
      this.studentCode,
      this.checkin,
      this.startDate,
      this.endDate,
      this.scheduleId,
      this.workDayId,
      });

  Students copyWith() => Students(
        timeCheckin: this.timeCheckin,
        timeCheckout: this.timeCheckout,
        nameStudent: this.nameStudent,
        className: this.className,
        classId: this.classId,
        studentCode: this.studentCode,
        checkin: this.checkin,
        startDate: this.startDate,
        endDate: this.endDate,
        scheduleId: this.scheduleId,
        workDayId: this.workDayId,
      );

  factory Students.fromJson(Map<String, dynamic> json) => _$StudentsFromJson(json);

  Map<String, dynamic> toJson() => _$StudentsToJson(this);
}
