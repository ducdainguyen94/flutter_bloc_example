import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/attendance.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/students.dart';
import 'package:json_annotation/json_annotation.dart';

part 'shift.g.dart';

@JsonSerializable()
class Shift {
  Attendance? attendance;
  int? classId;
  String? className;
  String? endDate;
  String? nameTeacher;
  String? scheduleName;
  String? startDate;
  List<Students>? students;

  Shift(
      {this.attendance, this.classId, this.className, this.endDate, this.nameTeacher, this.scheduleName, this.startDate, this.students});

  factory Shift.fromJson(Map<String, dynamic> json) => _$ShiftFromJson(json);

  Map<String, dynamic> toJson() => _$ShiftToJson(this);
}


