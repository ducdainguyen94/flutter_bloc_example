// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shift.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Shift _$ShiftFromJson(Map<String, dynamic> json) => Shift(
      attendance: json['attendance'] == null
          ? null
          : Attendance.fromJson(json['attendance'] as Map<String, dynamic>),
      classId: json['classId'] as int?,
      className: json['className'] as String?,
      endDate: json['endDate'] as String?,
      nameTeacher: json['nameTeacher'] as String?,
      scheduleName: json['scheduleName'] as String?,
      startDate: json['startDate'] as String?,
      students: (json['students'] as List<dynamic>?)
          ?.map((e) => Students.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ShiftToJson(Shift instance) => <String, dynamic>{
      'attendance': instance.attendance,
      'classId': instance.classId,
      'className': instance.className,
      'endDate': instance.endDate,
      'nameTeacher': instance.nameTeacher,
      'scheduleName': instance.scheduleName,
      'startDate': instance.startDate,
      'students': instance.students,
    };
