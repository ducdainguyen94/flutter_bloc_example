import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/shift.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/shift_list_screen.dart';
import 'package:flutter/material.dart';

class ShiftListState {
  bool loading;
  String textSearch;
  List<Shift> listShift;
  List<Shift> listShiftCurent;
  bool showFilter;
  String? startDay;
  String? endDay;
  List<String> listClass;
  String? classSelected;
  String? stringSearch;
  int? classId;
  String? nameTeacher;
  Shift? shiftSelected;

  ShiftListState({
    required this.loading,
    required this.listShift,
    required this.listShiftCurent,
    required this.textSearch,
    required this.showFilter,
    this.startDay,
    this.endDay,
    required this.listClass,
    this.classSelected,
    this.stringSearch,
    this.classId,
    this.nameTeacher,
    this.shiftSelected,
  });
  
  ShiftListState copyWith({
    bool? loading,
    String? textSearch,
    List<Shift>? listShift,
    List<Shift>? listShiftCurent,
    bool? showFilter,
    String? startDay,
    String? endDay,
    List<String>? listClass,
    String? classSelected,
    String? stringSearch,
    int? classId,
    String? nameTeacher,
   
    Shift? shiftSelected,
  }) =>
      ShiftListState(
        loading: loading ?? this.loading,
        textSearch: textSearch ?? this.textSearch,
        listShift: listShift ?? this.listShift,
        listShiftCurent: listShiftCurent ?? this.listShiftCurent,
        showFilter: showFilter ?? this.showFilter,
        startDay: startDay ?? this.startDay,
        endDay: endDay ?? this.endDay,
        listClass: listClass ?? this.listClass,
        
        classSelected: classSelected ?? this.classSelected,
        stringSearch: stringSearch ?? this.stringSearch,
        classId: classId ?? this.classId,
        nameTeacher: nameTeacher,
        shiftSelected: shiftSelected ?? this.shiftSelected
      );
}
