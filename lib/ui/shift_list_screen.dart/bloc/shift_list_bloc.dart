import 'package:base_flutter_intlabs/app.dart';
import 'package:base_flutter_intlabs/inject_dependency/injection.dart';
import 'package:base_flutter_intlabs/models/response/base_api_response.dart';
import 'package:base_flutter_intlabs/network/api.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/bloc/detail_shift_bloc.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/bloc/shift_list_state.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/model/shift.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/instance_manager.dart';
import 'package:intl/intl.dart';
import 'package:loader_overlay/loader_overlay.dart';

class ShiftListBloc extends Cubit<ShiftListState> {
  ShiftListBloc()
      : super(ShiftListState(
          loading: false,
          listShift: [],
          listShiftCurent:[],
          textSearch: '',
          showFilter: true,
          listClass: [],
          startDay: DateTime(DateTime.now().subtract(const Duration(days: 7)).year, DateTime.now().subtract(const Duration(days: 7)).month,
                  DateTime.now().subtract(const Duration(days: 7)).day)
              .toString(),
          endDay: DateTime.now().toString(),

          // startDay: DateFormat('dd-MM-yyyy').format(DateTime(DateTime.now().subtract(const Duration(days: 7)).year, DateTime.now().subtract(const Duration(days: 7)).month, DateTime.now().subtract(const Duration(days: 7)).day)),
          // endDay: DateFormat("dd-MM-yyyy").format(DateTime.now()),
          classId: null,
          stringSearch: null,
          classSelected: "Tất cả"
        ));

// lấy danh sách ca dạy
getListClassOfTeacher() async {
    try {
      Map<String, dynamic> params = {
        "classId": state.classId ,
        "keySearch": state.stringSearch == "" ? null : state.stringSearch,
          "startDate": DateTime(DateTime.parse(state.startDay!).year ,DateTime.parse(state.startDay!).month, DateTime.parse(state.startDay!).day,00, 00 , 00 ).toIso8601String(),
          "endDate": DateTime(DateTime.parse(state.endDay!).year ,DateTime.parse(state.endDay!).month, DateTime.parse(state.endDay!).day,23, 59 , 59 ).toIso8601String(),
      };
      var res = await getIt.get<Api>().getListShiftOfTeacher(params);

      final result = BaseApiResponse.fromJson(res); // Chỉ làm mục đích đưa chuỗi Json --> List

      if (result.success) {

        List<Shift> listShift = (result.data as List).map((e) => Shift.fromJson(e)).toList();
        final List<String> listClass = [];
        listShift.forEach((e) {
          listClass.add(e.className!);
        });
        listClass.add("Tất cả");

        var handleListShift = listClass.toSet().toList();
        emit(state.copyWith(listClass: handleListShift,listShiftCurent: listShift));
        
      } else {
        print('ERROR=> ');
      }
    } catch (e) {
      print('error--- $e');
    }
  }
  // Lấy Danh sách ca dạy cho giáo viên
  getListShiftOfTeacher() async {
    try {
      print('CALL -------GET LIST SHIFT OF TEACHER====> ${state.classId}');
    var now = DateTime.now();
    var startDate = DateTime(now.year, now.month, now.day);
    var endDate = DateTime(now.year, now.month, now.day + 1);

      Map<String, dynamic> params = {
        // "classId": state.classId == 0 ? null : state.classId ,
        // "keySearch": state.stringSearch == "" ? null : state.stringSearch,

        // "startDate": "2022-12-20T07:35:00.906Z",
        // "endDate": "2022-12-28T07:35:00.906Z",

        // "startDate": DateTime.parse(state.startDay!).toIso8601String(),
        // "endDate": DateTime.parse(state.endDay!).toIso8601String(),

          // "startDate": DateTime(DateTime.parse(state.startDay!).year ,DateTime.parse(state.startDay!).month, DateTime.parse(state.startDay!).day,00, 00 , 00 ).toIso8601String(),
          // "endDate": DateTime(DateTime.parse(state.endDay!).year ,DateTime.parse(state.endDay!).month, DateTime.parse(state.endDay!).day,23, 59 , 59 ).toIso8601String(),
          "endDate":"2023-02-03T16:59:59.806Z",
          "startDate":"2023-02-02T17:00:00.806Z"
       

        // "startDate": DateFormat('dd-MM-yyyy').parse(state.startDay!).toIso8601String(),
        // "endDate": DateFormat('dd-MM-yyyy').parse(state.endDay!).toIso8601String(),
      };
      print('GET LIST SHIFT OF TEACHER  $params' );
      navigatorKey.currentContext!.loaderOverlay.show();
      var res = await getIt.get<Api>().getListShiftOfTeacher(params);
      final result = BaseApiResponse.fromJson(res); // Chỉ làm mục đích đưa chuỗi Json --> List
      print(' RESULT________________result.toJson():  ${result.toJson()}'); // Cái này chỉ để in ra để nhìn thôi
      if (result.success) {
        List<Shift> listShift = (result.data as List).map((e) => Shift.fromJson(e)).toList();
        // List<Shift> listShift = listShiftNotSort.sort((a, b) => DateTime.parse(a.startDate!)..compareTo(DateTime.parse(b.startDate!)));

        // listShift.sort(((a, b) => a.scheduleName!.compareTo(b.scheduleName!)));
        listShift.sort((a,b) => DateTime.parse(a.startDate!).compareTo(DateTime.parse(b.startDate!)));
        
        if(state.shiftSelected == null){
          print('CALL -------GET LIST SHIFT OF TEACHER==> ${state.classId}');
          emit(state.copyWith(listShift: listShift));
        }else{
          Shift item = listShift.firstWhere((e) => (e.classId == state.shiftSelected!.classId && e.scheduleName == state.shiftSelected!.scheduleName && e.startDate == state.shiftSelected!.startDate));
          emit(state.copyWith(listShift: listShift, shiftSelected: item));
          detailScreenKey.currentContext!.read<DetailShiftBloc>().getListStudentOfClassShift(item);
        }   
      } else {
        print('ERROR=> ');
      }
      navigatorKey.currentContext!.loaderOverlay.hide();
    } catch (e) {
      navigatorKey.currentContext!.loaderOverlay.hide();
      print('error--- $e');
    }
  }

  // Lấy danh sách ca học cho phụ huynh xem
  getListShiftOfParent() async {
    try {
      Map<String, dynamic> params = {
        "classId": 0,
        // "endDate": "2022-12-28T07:35:00.906Z",
        "isCheckin": null,
        // "startDate" :"2022-12-20T07:35:00.906Z"
      };
      print('GET LIST SHIFT OF TEACHER  $params');

      var res = await getIt.get<Api>().getListShiftOfParent(params);

      final result = BaseApiResponse.fromJson(res); // Chỉ làm mục đích đưa chuỗi Json --> List
      print(' RESULT________________result.toJson():  ${result.toJson()}'); // Cái này chỉ để in ra để nhìn thôi
      print(' RESULT________________success :  ${result.success}');
      print(' RESULT________________data.toString() : ${result.data.toString()}');

      if (result.success) {
        print('RESULT SUCCESS______ ');
        // List<Shift> listShift = (result.data as List).map((e) => Shift.fromJson(e)).toList();
        // emit(state.copyWith(listShift: listShift));
        print('RESULT SUCCESS______ done');
        // print('RESULT SUCCESS______ done ${listShift[0].students![0].toJson()}');

      } else {
        print('ERROR=> ');
      }
    } catch (e) {
      print('error--- $e');
    }
  }

  //set item shift selected
  void setItemSelected(Shift? item){
    emit(state.copyWith(shiftSelected: item));
  }

  //show view filter : bật tắt filter
  void showViewFilter() {
    emit(state.copyWith(showFilter: !state.showFilter));
  }

  // on change Day : Đổi ngày chọn
  void onChangeDay(String? startDay, String? endDay) {
    emit(state.copyWith(startDay: startDay, endDay: endDay));
  }

  //onchange class
  void onChangeClass(String? value) {
    print("Change -----------classSelected $value ");
    emit(state.copyWith(classSelected: value));

    // print("Change -----------classSelected $classSelected ");


    if (value == 'Tất cả') {
      print("Change Tất cả----------- ");
      emit(state.copyWith(classId: 0));
      print("Change class All -----------  ${state.classId} ");
    } else {
      print("Change Không phải Tất cả----------- ");
      print(state.listShiftCurent);
     List<Shift> listFilter = state.listShiftCurent.where((e) => e.className == value).toList();
     print(listFilter);
    if(listFilter.isNotEmpty){
      emit(state.copyWith(classId: listFilter[0].classId));
    }
      // state.listShiftCurent.forEach((e) {
      //   if (e.className == value) {
      //     emit(state.copyWith(classId: e.classId));
      //     print("Change Không phải Tất cả-----------  ${state.classId}");
      //     return;
      //   }
      // });
    }
    print("Change class +++++++++  ${state.classId} ");
  }

  // Thay đổi string ô tìm kiếm
  void onChangeStringSearch(String? value) {
    emit(state.copyWith(stringSearch: value));
  }
}
