class LoginState {
  String account;
  String password;
  bool showPass;
  bool loading;
  bool isSaveLogIn;
  Map<String, dynamic> saveLogIn;

  LoginState({required this.account, required this.password, required this.showPass, this.loading = false, required this.isSaveLogIn, required this.saveLogIn});

  LoginState copyWith({String? account, String? password, bool? showPass, bool? loading, bool? isSaveLogIn,  Map<String, dynamic>? saveLogIn}) => LoginState(
      account: account ?? this.account,
      password: password ?? this.password,
      showPass: showPass ?? this.showPass,
      loading: loading ?? this.loading,
      isSaveLogIn: isSaveLogIn ?? this.isSaveLogIn,
      saveLogIn: saveLogIn ?? this.saveLogIn
      );
}