import 'dart:async';
import 'dart:convert';

import 'package:base_flutter_intlabs/app.dart';
import 'package:base_flutter_intlabs/blocs/app_bloc.dart';
import 'package:base_flutter_intlabs/database/shared_preference_helper.dart';
import 'package:base_flutter_intlabs/inject_dependency/injection.dart';
import 'package:base_flutter_intlabs/models/entities/user/user_entity.dart';
import 'package:base_flutter_intlabs/models/response/base_api_response.dart';
import 'package:base_flutter_intlabs/network/api.dart';
import 'package:base_flutter_intlabs/router/router_name.dart';
import 'package:base_flutter_intlabs/ui/auth/login/bloc/login_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/instance_manager.dart';
import 'package:get/get.dart';

class LoginBloc extends Cubit<LoginState> {
  LoginBloc()
      : super(LoginState(account: '', password: '', showPass: false, isSaveLogIn: false, saveLogIn: {"account": "", "passWord": "", "isSaveLogIn": ""}));

  //account or password change
  void changeText(String txt, bool isAccount) {
    emit(state.copyWith(
      account: isAccount ? txt : state.account,
      password: !isAccount ? txt : state.password,

    ));

  
  }

  //toggle show pass
  void toggleShowPass() {
    emit(state.copyWith(showPass: !state.showPass));
  }

  void toggleSaveLogIn() {
    emit(state.copyWith(isSaveLogIn: !state.isSaveLogIn));
    print("decodeMap-----state.IsSaveLogIn ${state.isSaveLogIn}");
  }

  void getLogInFromShare(TextEditingController edtUser, TextEditingController edtPass) async {
    String? saveLogIn = await getIt.get<SharedPreferenceHelper>().getLogIn;
    Map<String, dynamic> decodeMap = json.decode(saveLogIn!);
    print("decodeMap $decodeMap");
    edtUser.text = decodeMap['account'];
    edtPass.text = decodeMap['passWord'];  // show to view user seen
    emit(state.copyWith(isSaveLogIn:decodeMap['isSaveLogIn'] ));
    emit(state.copyWith(account: decodeMap['account']));   // Push state to service login
    emit(state.copyWith(password: decodeMap['passWord']));
  }

  //validate user and password
  void validateField() {
    print('okkfkkf');
    if (state.account.trim().isEmpty) {
      Fluttertoast.showToast(
          msg: "Vui lòng nhập tên tài khoản",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
      return;
    } else if (state.password.trim().isEmpty) {
      Fluttertoast.showToast(
          msg: "Vui lòng nhập mật khẩu",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
      return;
    }
    login();

    emit(state.copyWith(saveLogIn: {"account": state.account, "passWord": state.password, "isSaveLogIn": state.isSaveLogIn}));
    print("decodeMap-----change state in Bloc when onPress login ${json.encode(state.saveLogIn)}");

    if (state.isSaveLogIn == true) {
      String encodeMap = json.encode(state.saveLogIn);
      getIt.get<SharedPreferenceHelper>().saveLogIn(encodeMap);
      print("decodeMap----saveEncodeMap $encodeMap");
    } else {
      getIt.get<SharedPreferenceHelper>().removeLogIn(); // Nếu isSaveLogIn == false  thì sẽ không lưu mà xoá hết dữ liệu ở trong SharePreferences
      print("decodeMap------removeMap ");
    }
  }

  //login app
  void login() async {
    if (state.loading) return;
    emit(state.copyWith(loading: true));
    try {
      var res = await getIt.get<Api>().loginApp({'grant_type': 'password', 'scope': 'read', 'username': state.account, 'password': state.password});
      final result = BaseApiResponse.fromJson(res);
      print('RESULT===CHECK> ${result.toJson()}');
      if (result.success) {
        UserEntity user = UserEntity.fromJson(result.data);
        print('USER;;;; ${user.toJson()}');
        //call api subscrible topic firebase
        subscribleTopicFirebase();
        //save data
        navigatorKey.currentContext!.read<AppBloc>().saveUserAuthen(user);
        getIt.get<SharedPreferenceHelper>().saveAuthToken(user.access_token!);
        getIt.get<SharedPreferenceHelper>().saveOrganizationId(user.organization_id!);
        Get.offAllNamed(RouteName.shiftListScreen);
      } else {
        Fluttertoast.showToast(
            msg: result.message ?? "Đăng nhập lỗi, vui lòng thử lại",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.TOP,
            timeInSecForIosWeb: 3,
            backgroundColor: Colors.white,
            textColor: Colors.black,
            fontSize: 16.0);
      }
      emit(state.copyWith(loading: false));
    } catch (e) {
      print('CATCH $e');
      Fluttertoast.showToast(
          msg: "Đăng nhập lỗi, vui lòng thử lại",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 3,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
      emit(state.copyWith(loading: false));
    }
  }

  //subscrible topic firebse
  void subscribleTopicFirebase() async {
    try {
      var tokenFcm = getIt.get<SharedPreferenceHelper>().getTokenFcm;
      var res = await getIt.get<Api>().subscribleTopic({
        'tokens': [tokenFcm],
      });
      final result = BaseApiResponse.fromJson(res);
      print('RESULT SUBSCRIBE TOKEN FIREBASE=== ${result.toJson()}');
      if (result.success) {
      } else {
        print('ERROR=> ');
      }
    } catch (e) {
      print('CATCH $e');
    }
  }
}
