import 'dart:convert';

import 'package:base_flutter_intlabs/blocs/app_bloc.dart';
import 'package:base_flutter_intlabs/database/shared_preference_helper.dart';
import 'package:base_flutter_intlabs/inject_dependency/injection.dart';
import 'package:base_flutter_intlabs/router/auth_service.dart';
import 'package:base_flutter_intlabs/router/router_name.dart';
import 'package:base_flutter_intlabs/ui/auth/login/bloc/login_bloc.dart';
import 'package:base_flutter_intlabs/ui/auth/login/bloc/login_state.dart';
import 'package:base_flutter_intlabs/ui/auth/login/widget/clippath_header.dart';
import 'package:base_flutter_intlabs/widgets/button_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isChecked = false;
  final _accountController = TextEditingController();
  final _passWordController = TextEditingController();
  Map<String, String> decodeMap = {};

  @override
  void initState() {
    super.initState();
    context.read<LoginBloc>().getLogInFromShare(_accountController, _passWordController);
    // _accountController.text = context.read<LoginBloc>().state.saveLogIn["account"];
    // _passWordController.text = context.read<LoginBloc>().state.saveLogIn["passWord"];   // Not Action
    print("decodeMap------ state get from share preference ${context.read<LoginBloc>().state.saveLogIn["account"].toString()}");
  }

  @override
  Widget build(BuildContext context) {
    print('Render login');
    // TODO: implement build
    return GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
            body: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) => Container(
            // padding: EdgeInsets.all(20),
            child: Column(
              children: [
                ClipPathHeader(),
                Expanded(
                  child: Container(
                      margin: EdgeInsets.only(top: 20.h),
                      padding: EdgeInsets.only(left: 30.w, right: 30.w),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            TextField(
                              controller: _accountController,
                              decoration: InputDecoration(
                                  hintText: 'Tài khoản',
                                  suffixIcon: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      IconButton(
                                        icon: Icon(
                                          Icons.person,
                                        ),
                                        onPressed: () {
                                          // cbChangeShowPass();
                                        },
                                      )
                                    ],
                                  )),
                              onChanged: (value) => {
                                context.read<LoginBloc>().changeText(value, true)
                                // context.read<LoginBloc>().emit(state.copyWith(saveLogin: {"account": state.account, "passWord": state.password, "isSaveLogIn": state.isSaveLogIn}))
                              },
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            TextField(
                              obscureText: state.showPass,
                              controller: _passWordController,
                              decoration: InputDecoration(
                                  hintText: 'Mật khẩu',
                                  suffixIcon: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      IconButton(
                                        icon: Icon(
                                          !state.showPass ? Icons.visibility_off : Icons.visibility,
                                        ),
                                        onPressed: () {
                                          context.read<LoginBloc>().toggleShowPass();
                                        },
                                      )
                                    ],
                                  )),
                              onChanged: (value) => {context.read<LoginBloc>().changeText(value, false)},
                            ),
                            SizedBox(
                              height: 20.h,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                           
                                Spacer(),
                                Checkbox(
                                  checkColor: Colors.white,
                                  // fillColor: Colors.amber,
                                  value: state.isSaveLogIn,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      // isChecked = value!;
                                      context.read<LoginBloc>().toggleSaveLogIn();
                                    });
                                  },
                                ),
                                Container(
                                  decoration: BoxDecoration(),
                                  // padding: EdgeInsets.only(right: 30),
                                  child: Text('Ghi nhớ đăng nhập'),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
                            return !isKeyboardVisible
                          ? ButtonCustom(
                                title: 'Đăng nhập',
                                loading: state.loading,
                                onClick: () {
                                  context.read<LoginBloc>().validateField();
                                }): Container();})
                                // Spacer(),
                                    //  InkWell(
                                    // onTap: () {
                                    //   print("SignUp---- đã bấm đăng ký");
                                    //   Navigator.pushNamed(context, RouteName.signUp);
                                    // },
                                    // child: Container(
                                    //   // color: Colors.amber.withOpacity(0.4),
                                    //  child: Text("Đăng ký tài khoản", style:  TextStyle(color: Colors.blue),)
                                    //  )),
                          ],
                        ),
                      )),
                ),
                    // InkWell(
                    //                 onTap: () {
                    //                   print("SignUp---- đã bấm đăng ký");
                    //                   Navigator.pushNamed(context, RouteName.signUp);
                    //                 },
                    //                 child: Container(
                    //                   // color: Colors.amber.withOpacity(0.4),
                    //                  child: Text("Đăng ký tài khoản", style:  TextStyle(color: Colors.blue),)
                    //                  )),
                  SizedBox(height: 30.h,)
              ],
            ),
          ),
        )));
  }
}
