import 'package:base_flutter_intlabs/common/app_images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class ClipPathHeader extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ClippathScreenState();
  }
}

class _ClippathScreenState extends State<ClipPathHeader>{
  var number = 0;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        child: Column(
          children: [
            ClipPath(
              clipper: ImageClipper(),
              child: Image.asset(AppImages.imgBgLogin, width: double.infinity, height: MediaQuery.of(context).size.height/2.5, fit: BoxFit.cover,)
            ),
          ],
        )
      );
  }
}

class ImageClipper extends CustomClipper<Path> {
  @override
  getClip(Size size) {
      // TODO: implement getClip
      print('SIZE DAY ${size.width} ${size.height}');
      var controlPoint1 = Offset(100, size.height - 100);
      var controlPoint2 = Offset(size.width - 100, size.height + 50);
      var controlPoint3 = Offset(size.width/2, size.height/2 - 25);
      var controlPoint4 = Offset(size.width, size.height/2);
      var controlPoint5 = Offset(size.width/2, size.height/2);
      Path path = Path();
      path.lineTo(0, size.height);
      path.cubicTo(size.width/1.6, size.height - 30, (size.width/4)*2.7, 170, size.width, 200);
      path.lineTo(size.width, size.height - 50);
      path.lineTo(size.width, 0);
      path.close();
      // path.cubicTo(controlPoint1.dx, controlPoint1.dy, controlPoint2.dx, controlPoint2.dy, size.width, size.height - 50);
      // // path.conicTo(controlPoint3.dx, controlPoint3.dy, size.width, 0, 1);
      // path.arcToPoint(controlPoint4, radius: Radius.circular(50));
      // path.lineTo(size.width/2, size.height/2);
      // // path.arcToPoint(controlPoint5, radius: Radius.circular(500), clockwise: false);
     
      // // path.lineTo(size.width, 0);
      // // path.lineTo(0, size.height);
      // path.close();
      // Path path1 = Path();
      // path1.addOval(Rect.fromCircle(center: controlPoint5, radius: 50));
      // final combine = Path.combine(PathOperation.difference, path, path1);
      // return combine;
      return path;
    }
  
    @override
    bool shouldReclip(covariant CustomClipper oldClipper) {
      print('CO GOI shouldReclip ${oldClipper}');
    // TODO: implement shouldReclip
    return false;
  }
  
}
