import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:base_flutter_intlabs/app.dart';
import 'package:base_flutter_intlabs/database/shared_preference_helper.dart';
import 'package:base_flutter_intlabs/router/auth_service.dart';
// import 'package:base_flutter_intlabs/ui/tab_dashboard/bloc/dashboard_bloc.dart';
// import 'package:base_flutter_intlabs/ui/tab_list_checkin/bloc/list_checkin_bloc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import '../inject_dependency/injection.dart';

class NotificationService {
  /// We want singelton object of ``NotificationService`` so create private constructor
  /// Use NotificationService as ``NotificationService.instance``
  NotificationService._internal();

  static final NotificationService instance = NotificationService._internal();

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  // FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
  //     new FlutterLocalNotificationsPlugin();

  /// For local_notification id
  int _count = 0;

  /// ``NotificationService`` started or not.
  /// to start ``NotificationService`` call start method
  bool _started = false;

  /// Call this method on startup
  /// This method will initialise notification settings
  void start() {
    if (!_started) {
      _integrateNotification();
      _getToken();
      _started = true;
    }
  }

  // Call this method to initialize notification
  void _integrateNotification() {
    print('token=======>:ok');
    _registerNotification();
    // _initializeLocalNotification();
  }

  /// initialize firebase_messaging plugin
  void _registerNotification() async {
    NotificationSettings settings = await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      //On tap notification init app
      FirebaseMessaging.instance
          .getInitialMessage()
          .then((RemoteMessage? message) {
        if (message != null) {
          // Timer(Duration(milliseconds: 300), ()=> navigatorKey.currentContext!.read<NotificationListBloc>().processNotificationTab(3, data: message.data));

        }
      });

      //On notification foreground
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        RemoteNotification notification = message.notification!;

        //show notification local
        // if(Platform.isAndroid){
        //   AndroidNotification android = message.notification?.android as AndroidNotification;
        //   if (notification != null && android != null) {
        //     print('ON MESSAGE====> ON MESSAGE FOREGROUND');
        //     // _showNotification(notification);
        //     _showNotificationWithNoSound(notification);
        //   }
        // } else {
        //   print('ON MESSAGE====> CALL');
        //   _showNotificationWithNoSound(notification);
        // }
        print('ON MESSAGE====>CM ON MESSAGE FOREGROUND ${notification}');
        print('ON MESSAGE====>CM ON MESSAGE FOREGROUND ${message.data}');
        print('ON MESSAGE====>CM ON MESSAGE FOREGROUND ${notification.title}');
        print('ON MESSAGE====>CM ON MESSAGE FOREGROUND ${notification.body}');

        //process count new message
        if (message.data != null &&
            notification.title == 'Ghi nhận khách hàng mới' &&
            Get.find<AuthService>().isAuth == true) {
          // listCheckInScreenKey.currentContext!
          //     .read<ListCheckInBloc>()
          //     .onNotificationCheckIn(message.data);
          // listCheckInScreenKey.currentContext!.read<DashboardBloc>().onNotificationCheckIn(message.data);
        }
      });

      //On tap Notification when app background
      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        print('A new onMessageOpenedApp event was published!');
        print('ON MESSAGE====> OPEN APP ${message.data}');
        //process count new message
        // navigatorKey.currentContext!.read<NotificationListBloc>().processNotificationTab(2, data: message.data);
      });
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }

    /// App in foreground -> [onMessage] callback will be called
    /// App terminated -> Notification is delivered to system tray. When the user clicks on it to open app [onLaunch] fires
    /// App in background -> Notification is delivered to system tray. When the user clicks on it to open app [onResume] fires
    // _firebaseMessaging.(
    //   onMessage: _onMessage,
    //   onLaunch: _onLaunch,
    //   onResume: _onResume,
    // );
    _firebaseMessaging.onTokenRefresh
        .listen(_tokenRefresh, onError: _tokenRefreshFailure);
  }

  /// Token is unique identity of the device.
  /// Token is required when you want to send notification to perticular user.
  void _getToken() {
    _firebaseMessaging.getToken().then((token) async {
      print('token=======>: $token');
      //save tokenFirebase
      getIt.get<SharedPreferenceHelper>().saveTokenFcm(token!);
    }, onError: _tokenRefreshFailure);
  }

  /// This method will be called device token get refreshed
  void _tokenRefresh(String newToken) async {
    print('New Token : $newToken');
    //save tokenFirebase new
    // getIt.get<SharedPreferenceHelper>().saveTokenFcm(newToken);

    // var currentUserString = getIt.get<SharedPreferenceHelper>().jwtToken;
    // if (currentUserString != null && currentUserString.isNotEmpty) {
    //   try {
    //     var res = await getIt.get<AuthApi>().saveTokenFirebase(newToken);
    //     if (res?['error'] != null && res['error'].toString().isNotEmpty) {
    //       print('SUBCRIBE TOKEN FCM FAILSE $res');
    //       return;
    //     }
    //     print('SUBCRIBE TOKEN FCM SUCCESS $res');
    //   } catch (e) {
    //     print('SUBCRIBE TOKEN FCM FAILSE ${e.toString()}');
    //   }
    // }
  }

  void _tokenRefreshFailure(error) {
    print("FCM token refresh failed with error $error");
  }

  /// This method will be called on tap of the notification which came when app was in foreground
  ///
  /// Firebase messaging does not push notification in notification panel when app is in foreground.
  /// To send the notification when app is in foreground we will use flutter_local_notification
  /// to send notification which will behave similar to firebase notification
  Future<void> _onMessage(Map<String, dynamic> message) async {
    print('onMessage: $message');
    if (Platform.isIOS) {
      message = _modifyNotificationJson(message);
    }
    // _showNotification(
    //   {
    //     "title": message['notification']['title'],
    //     "body": message['notification']['body'],
    //     "data": message['data'],
    //   },
    // );
  }

  /// This method will be called on tap of the notification which came when app was closed
  Future<void>? _onLaunch(Map<String, dynamic> message) {
    print('onLaunch: $message');
    if (Platform.isIOS) {
      message = _modifyNotificationJson(message);
    }
    _performActionOnNotification(message);
    return null;
  }

  /// This method will be called on tap of the notification which came when app was in background
  Future<void>? _onResume(Map<String, dynamic> message) {
    print('onResume: $message');
    if (Platform.isIOS) {
      message = _modifyNotificationJson(message);
    }
    _performActionOnNotification(message);
    return null;
  }

  /// This method will modify the message format of iOS Notification Data
  Map<String, dynamic> _modifyNotificationJson(Map<String, dynamic> message) {
    message['data'] = Map.from(message);
    message['notification'] = message['aps']['alert'];
    return message;
  }

  /// We want to perform same action of the click of the notification. So this common method will be called on
  /// tap of any notification (onLaunch / onMessage / onResume)
  void _performActionOnNotification(Map<String, dynamic> message) {
    // NotificationsBloc.instance.newNotification(message);
  }

  /// initialize flutter_local_notification plugin
  // void _initializeLocalNotification() async{
  //   // Settings for Android
  //   const AndroidInitializationSettings initializationSettingsAndroid =
  //       AndroidInitializationSettings('@mipmap/ic_launcher');

  //   // Settings for iOS
  //   /// Note: permissions aren't requested here just to demonstrate that can be
  //   /// done later
  //   final IOSInitializationSettings initializationSettingsIOS =
  //       IOSInitializationSettings(
  //           requestAlertPermission: false,
  //           requestBadgePermission: false,
  //           requestSoundPermission: false,
  //           onDidReceiveLocalNotification:
  //               (int id, String? title, String? body, String? payload) async {
  //                 UtilLogger.log("");
  //           });
  //   const MacOSInitializationSettings initializationSettingsMacOS =
  //       MacOSInitializationSettings(
  //           requestAlertPermission: false,
  //           requestBadgePermission: false,
  //           requestSoundPermission: false);

  //   final InitializationSettings initializationSettings = InitializationSettings(
  //       android: initializationSettingsAndroid,
  //       iOS: initializationSettingsIOS,
  //       macOS: initializationSettingsMacOS);
  //     await _flutterLocalNotificationsPlugin.initialize(initializationSettings,
  //       onSelectNotification: (String? payload) async {
  //     if (payload != null) {
  //       print('DU LIEU DAY ROI ');
  //     }
  //   });
  // }

  /// This method will be called on tap of notification pushed by flutter_local_notification plugin when app is in foreground
  Future<void>? _onSelectLocalNotification(String payLoad) {
    Map data = json.decode(payLoad);
    Map<String, dynamic> message = {
      "data": data,
    };
    print('Tap notification local ');
    // _performActionOnNotification(message);
    return null;
  }

  //show notification local
  // Future<void> _showNotificationWithNoSound(RemoteNotification notification) async {
  //   const AndroidNotificationDetails androidPlatformChannelSpecifics =
  //       AndroidNotificationDetails('silent channel id', 'silent channel name',
  //           playSound: false,
  //           styleInformation: DefaultStyleInformation(true, true));
  //   const IOSNotificationDetails iOSPlatformChannelSpecifics =
  //       IOSNotificationDetails(presentSound: false);
  //   const MacOSNotificationDetails macOSPlatformChannelSpecifics =
  //       MacOSNotificationDetails(presentSound: false);
  //   const NotificationDetails platformChannelSpecifics = NotificationDetails(
  //       android: androidPlatformChannelSpecifics,
  //       iOS: iOSPlatformChannelSpecifics,
  //       macOS: macOSPlatformChannelSpecifics);
  //   await _flutterLocalNotificationsPlugin.show(0, notification.title,
  //       notification.body, platformChannelSpecifics,
  //       // payload: json.encode(
  //       //   message['data'],
  //       // ),
  //     );
  // }
}
