import 'package:flutter/material.dart';

class AppLanguage {
  //default language
  static Locale defaultLanguage = Locale('vi');

  //list language support
  static List<Locale> supportLanguage = [
    Locale('vi'),
    Locale('en')
  ];

  //singleton factory
  AppLanguage._internal();
  static final AppLanguage _instance = AppLanguage._internal();
  factory AppLanguage(){
    return _instance;
  }
}