import 'package:json_annotation/json_annotation.dart';

part 'image_customer.g.dart';
@JsonSerializable()
class ImageCustomer {
  String? fileName;
  String? url;

  ImageCustomer({this.fileName, this.url});

  factory ImageCustomer.fromJson(Map<String, dynamic> json) => _$ImageCustomerFromJson(json);

  Map<String, dynamic> toJson() => _$ImageCustomerToJson(this);
}