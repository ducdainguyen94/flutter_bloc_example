// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_customer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageCustomer _$ImageCustomerFromJson(Map<String, dynamic> json) =>
    ImageCustomer(
      fileName: json['fileName'] as String?,
      url: json['url'] as String?,
    );

Map<String, dynamic> _$ImageCustomerToJson(ImageCustomer instance) =>
    <String, dynamic>{
      'fileName': instance.fileName,
      'url': instance.url,
    };
