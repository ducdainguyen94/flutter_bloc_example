import 'package:json_annotation/json_annotation.dart';

part 'event.g.dart';

@JsonSerializable()
class Event {
  int id;
  String? nameVi;
  String? nameEn;
  String? startDate;
  String? endDate;
  String? location;
  int? organizationId;
  String? imageUrl;

  Event(
      {required this.id,
      this.nameVi,
      this.nameEn,
      this.startDate,
      this.endDate,
      this.location,
      this.organizationId,
      this.imageUrl});

  factory Event.fromJson(Map<String, dynamic> json) => _$EventFromJson(json);

  Map<String, dynamic> toJson() => _$EventToJson(this);
}
