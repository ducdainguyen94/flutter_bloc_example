// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Event _$EventFromJson(Map<String, dynamic> json) => Event(
      id: json['id'] as int,
      nameVi: json['nameVi'] as String?,
      nameEn: json['nameEn'] as String?,
      startDate: json['startDate'] as String?,
      endDate: json['endDate'] as String?,
      location: json['location'] as String?,
      organizationId: json['organizationId'] as int?,
      imageUrl: json['imageUrl'] as String?,
    );

Map<String, dynamic> _$EventToJson(Event instance) => <String, dynamic>{
      'id': instance.id,
      'nameVi': instance.nameVi,
      'nameEn': instance.nameEn,
      'startDate': instance.startDate,
      'endDate': instance.endDate,
      'location': instance.location,
      'organizationId': instance.organizationId,
      'imageUrl': instance.imageUrl,
    };
