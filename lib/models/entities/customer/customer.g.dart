// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Customer _$CustomerFromJson(Map<String, dynamic> json) => Customer(
      id: json['id'] as int,
      fullName: json['fullName'] as String,
      company: json['company'] as String?,
      phone: json['phone'] as String,
      email: json['email'] as String,
      imageStore: (json['imageStore'] as List<dynamic>?)
          ?.map((e) => ImageCustomer.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CustomerToJson(Customer instance) => <String, dynamic>{
      'id': instance.id,
      'fullName': instance.fullName,
      'company': instance.company,
      'phone': instance.phone,
      'email': instance.email,
      'imageStore': instance.imageStore,
    };
