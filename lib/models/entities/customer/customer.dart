
import 'package:base_flutter_intlabs/models/entities/image/image_customer.dart';
import 'package:json_annotation/json_annotation.dart';

part 'customer.g.dart';
@JsonSerializable()
class Customer {
  int id;
  String fullName;
  String? company;
  String phone;
  String email;
  List<ImageCustomer>? imageStore;

  Customer({required this.id, required this.fullName, this.company, required this.phone, required this.email,
  this.imageStore});

  factory Customer.fromJson(Map<String, dynamic> json) => _$CustomerFromJson(json);

  Map<String, dynamic> toJson() => _$CustomerToJson(this);
}