// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserEntity _$UserEntityFromJson(Map<String, dynamic> json) => UserEntity(
      user_id: json['user_id'] as int?,
      access_token: json['access_token'] as String?,
      organization_branch_id: json['organization_branch_id'] as int?,
      organization_acronym: json['organization_acronym'] as String?,
      email: json['email'] as String?,
      organization_id: json['organization_id'] as int?,
    );

Map<String, dynamic> _$UserEntityToJson(UserEntity instance) =>
    <String, dynamic>{
      'user_id': instance.user_id,
      'access_token': instance.access_token,
      'organization_branch_id': instance.organization_branch_id,
      'organization_id': instance.organization_id,
      'organization_acronym': instance.organization_acronym,
      'email': instance.email,
    };
