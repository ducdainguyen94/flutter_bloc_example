import 'package:json_annotation/json_annotation.dart';

part 'user_entity.g.dart';
@JsonSerializable()
class UserEntity {
  int? user_id;
  String? access_token;
  int? organization_branch_id;
  int? organization_id;
  String? organization_acronym;
  String? email;

  UserEntity({this.user_id, this.access_token, this.organization_branch_id, this.organization_acronym, this.email, this.organization_id});

  factory UserEntity.fromJson(Map<String, dynamic> json) => _$UserEntityFromJson(json);

  Map<String, dynamic> toJson() => _$UserEntityToJson(this);
}