import 'package:json_annotation/json_annotation.dart';

part 'notification_entity.g.dart';
@JsonSerializable()
class NotificationEntity {
  int id;
  String? title;

  NotificationEntity({required this.id, this.title});

  factory NotificationEntity.fromJson(Map<String, dynamic> json) => _$NotificationEntityFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationEntityToJson(this);
}