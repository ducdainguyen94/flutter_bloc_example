class BaseApiResponse {
  late int? code;
  dynamic data;
  dynamic message;
  late bool success;

  BaseApiResponse({required this.code, this.data, this.message,required this.success});

  BaseApiResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    data = json['data'];
    message = json['message'];
    success = json['success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['data'] = this.data;
    data['message'] = this.message;
    data['success'] = this.success;
    return data;
  }
}