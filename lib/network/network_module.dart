import 'dart:io';
import 'dart:convert';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:base_flutter_intlabs/database/shared_preference_helper.dart';
import 'package:base_flutter_intlabs/network/urls.dart';


abstract class NetworkModule {
  /// A singleton dio provider.
  ///
  /// Calling it multiple times will return the same instance.
  static Dio provideDioAuth(SharedPreferenceHelper sharedPrefHelper) {
    final dio = Dio();

    dio
      ..options.baseUrl = dotenv.env['BASE_LOGIN_URL'] ?? ''
      ..options.connectTimeout = Urls.connectionTimeout
      ..options.receiveTimeout = Urls.receiveTimeout
      ..options.headers = {'Content-Type': 'application/json; charset=utf-8'}
      ..interceptors.add(LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
        requestHeader: true,
      ))
      ..interceptors.add(
        InterceptorsWrapper(
          onRequest: (RequestOptions options,
              RequestInterceptorHandler handler) async {
            // getting token
            var token = await sharedPrefHelper.authToken;

            if (token != null) {
              options.headers.putIfAbsent('Authorization', () => 'Bearer ${token}');
            } else {
              print('Auth token is null================================>>>>>');
            }

            return handler.next(options);
          },
        ),
      );

      //check bad certificate
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      return client;
    };

    return dio;
  }

  /// Calling it multiple times will return the same instance.
  static Dio provideDio(SharedPreferenceHelper sharedPrefHelper) {
    final dio = Dio();

    dio
      ..options.baseUrl = dotenv.env['BASE_URL'] ?? ''
      ..options.connectTimeout = Urls.connectionTimeout
      ..options.receiveTimeout = Urls.receiveTimeout
      ..options.headers = {'Content-Type': 'application/json; charset=utf-8'}
      ..interceptors.add(LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
        requestHeader: true,
      ))
      ..interceptors.add(
        InterceptorsWrapper(
          onRequest: (RequestOptions options,
              RequestInterceptorHandler handler) async {
            // getting token
            var token = await sharedPrefHelper.authToken;

            if (token != null) {
              options.headers
                  .putIfAbsent('Authorization', () => 'Bearer ${token}');
              print('OPTIONS=======> ${options}');
            } else {
              // print('Auth token is null');
            }

            return handler.next(options);
          },
        ),
      );

    return dio;
  }

  //// server processor 5/10/2022
  /// Calling it multiple times will return the same instance.
  static Dio provideDioProcessor(SharedPreferenceHelper sharedPrefHelper) {
    final dio = Dio();

    dio
      ..options.baseUrl = dotenv.env['BASE_URL_PROCESSOR'] ?? ''
      ..options.connectTimeout = Urls.connectionTimeout
      ..options.receiveTimeout = Urls.receiveTimeout
      ..options.headers = {'Content-Type': 'application/json; charset=utf-8'}
      ..interceptors.add(LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
        requestHeader: true,
      ))
      ..interceptors.add(
        InterceptorsWrapper(
          onRequest: (RequestOptions options,
              RequestInterceptorHandler handler) async {
            // getting token
            var token = await sharedPrefHelper.authToken;

            if (token != null) {
              options.headers
                  .putIfAbsent('Authorization', () => 'Bearer ${token}');
              print('OPTIONS=======> ${options}');
            } else {
              // print('Auth token is null');
            }

            return handler.next(options);
          },
        ),
      );

    return dio;
  }
}
