import 'dart:async';
import 'dart:io';
import 'dart:convert';

import 'package:dio/dio.dart';

import 'package:base_flutter_intlabs/models/entities/user/user_entity.dart';
import 'package:base_flutter_intlabs/models/response/base_api_response.dart';
import 'package:base_flutter_intlabs/network/dio_client.dart';
import 'package:base_flutter_intlabs/network/urls.dart';

const authConfig = {
  'clientId': 'clientCamMobile',
  'clientSecret': 'n55pVJdXRpKM7btkQsbhZBddQS3mQUxra',
};

class Api {
  //Đây mới chỉ là khởi tạo 1 class để sau tạo các đối tượng call API
  // dio instance

  late final DioClient _dioAuth;
  late final DioClient _dioClient;
  late final DioClient _dioClientProcessor;

  // injecting dio instance
  Api(this._dioAuth, this._dioClient, this._dioClientProcessor);

  //LOGIN SCREEN=====================================
  //Login app
  Future<dynamic> loginApp(dynamic params) async {
    String encoded = base64.encode(utf8.encode('${authConfig['clientId']!}:${authConfig['clientSecret']!}'));
    return _dioAuth.post(Urls.login,
        data: params,
        options: Options(contentType: Headers.formUrlEncodedContentType, headers: {'Authorization': 'Basic ${encoded}', 'Accept': 'application/json'}));
  } // Bình thường là phải sài Bearer nhưng riêng login phải sài basic ở đây đã ghi đè options rồi

  //REGISTRY CUSTOMER SCREEN=====================================
  //API list Events
  Future<dynamic> getListEvents() async {
    return _dioClient.get(Urls.listEvents);
  }

  //API get listQuestions
  Future<dynamic> getListQuestions(dynamic params) async {
    return _dioClient.post(Urls.listQuestions, data: params);
  }

  //API subscrible topic
  Future<dynamic> subscribleTopic(dynamic params) async {
    return _dioClient.post(Urls.subscribeTopic, data: params);
  }

  //SCAN QR TO CHECKIN
  Future<dynamic> checkInfoCustomerById(int id) async {
    // print('PAR ${params}');
    return _dioClient.get('${Urls.checkInfoCustomerById}/${id}');
  }

  Future<dynamic> checkInfoCustomer(dynamic params) async {
    print('PAR ${params}');
    return _dioClient.get('${Urls.checkInfoCustomer}/${params}');
  }

  Future<dynamic> scanQrCheckIn(dynamic params) async {
    return _dioClient.post('${Urls.scanQrToCheckIn}/${params}');
  }

  // API registor add custom
  Future<dynamic> registorAddCustom(dynamic params) {
    return _dioClient.post(Urls.registorAddCustom, data: params);
  }

  //update image for customer
  Future<dynamic> updateImageCustomer(int id, dynamic params) {
    print('HEHKFHKFKFJF  ${Urls.updateImageCustomer}/${id}?images=${params}');
    return _dioClient.post('${Urls.updateImageCustomer}/${id}?images=${params}', data: {});
  }

  Future<dynamic> updateImageCustomerInGroup(int id, dynamic params) {
    print('HEHKFHKFKFJF  ${Urls.updateImageCustomerInGroup}/${id}?images=${params}');
    return _dioClient.post('${Urls.updateImageCustomerInGroup}/${id}?images=${params}', data: {});
  }

  Future<dynamic> uploadImageCustomerRepresent(dynamic params) {
    return _dioClient.post(Urls.uploadImageCustomer, data: params);
  }

  //API get info Exhibition company
  Future<dynamic> getInfoExhibitionCompany(dynamic params) async {
    return _dioClient.post(Urls.getInfoExhibitionCompany, data: params);
  }

  //API get info Exhibition customer
  Future<dynamic> getInfoExhibitionCustomer(dynamic params) async {
    return _dioClient.post(Urls.getInfoExhibitionCustomer, data: params);
  }

  //API get info Exhibition booth
  Future<dynamic> getInfoExhibitionBooth(dynamic params) async {
    return _dioClient.post(Urls.getInfoExhibitionBooth, data: params);
  }

  //API get info Exhibition check in
  Future<dynamic> getInfoExhibitionCheckIn(dynamic params) async {
    return _dioClientProcessor.post(Urls.getInfoExhibitionCheckIn, data: params);
  }

  //API call convert format IID to Image
  Future<dynamic> getFormatImage(dynamic params) async {
    return _dioClientProcessor.get('${Urls.getFormatImage}/${params}');
  }

  //print card
  Future<dynamic> printCardVisit(String qrcode) async {
    return _dioClient.post('${Urls.printCardVisit}/${qrcode}', data: {});
  }

  //get total check in

//API cũ không có truyền thời gian
  // Future<dynamic> getInfoExhibitionTotalCheckIn(int? eventId) async {
  //   if (eventId == null) {
  //     return _dioClient.post(Urls.getInfoExhibitionTotalCheckIn);
  //   } else {
  //     return _dioClient.post('${Urls.getInfoExhibitionTotalCheckIn}?entechEventId=$eventId');
  //   }
  // }

//API mới có truyền thời gian bên trong params
  Future<dynamic> getInfoExhibitionTotalCheckIn(Map params) async {
    return _dioClient.post(Urls.getInfoExhibitionTotalCheckIn, data: params);
  }

  // API registor account
  Future<dynamic> registorAccount(dynamic params) {
    return _dioAuth.post(Urls.registorAccount, data: params);
  }

  // API delete account
  Future<dynamic> deleteAccount() {
    return _dioAuth.post('${Urls.deleteAccount}', data: {});
  }

  //--------------- APP TEACHER -----------------

  //Get List shift of teacher
  Future<dynamic> getListShiftOfTeacher(dynamic params) async {
    return _dioClient.post(Urls.getListShiftOfTeacher, data: params);
  }

  //Get List shift of parents
  Future<dynamic> getListShiftOfParent(dynamic params) async {
    return _dioClient.post(Urls.getListShiftOfParent, data: params);
  }

  //Get info Exhibition booth
  Future<dynamic> getListStudentOfClassShift(dynamic params) async {
    return _dioClient.post(Urls.getListStudentOfClassShift, data: params);
  }

  // Điểm danh cho cho 1 học sinh
  Future<dynamic> actionAttendance(int params) async {
    print("params $params ");
    return _dioClient.post('${Urls.Attendance}/$params');
  }
}
