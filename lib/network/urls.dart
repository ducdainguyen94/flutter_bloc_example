class Urls {
  Urls._();
  // receiveTimeout
  static const int receiveTimeout = 15000;

  // connectTimeout
  static const int connectionTimeout = 30000;

  // auth
  static const String login = "/oauth/token";

  //registry customer
  static const String listEvents = "/api/v1/entech-event/find-by-organization";
  static const String listQuestions = "/api/v1/entech-question/find-by-condition";

  //subscribe topic
  static const String subscribeTopic = "/api/v1/notification/subscribe";

  //Scan qr to checkin
  static const String checkInfoCustomerById = "/api/v1/entech-customer/find-by-id";
  static const String checkInfoCustomer = "/api/v1/entech-customer/find-by-info";
  static const String scanQrToCheckIn = "/api/v1/entech-customer/check-in-qr-code";

  static const String updateImageCustomer = "/api/v1/entech-customer/update-avatar";
  static const String updateImageCustomerInGroup = "/api/v1/entech-customer/update-images";

  static const String registorAddCustom = "/api/v1/entech-customer/create";
  static const String uploadImageCustomer = "/api/v1/entech-customer/upload-file";

  static const String getInfoExhibitionCompany = "/api/v1/entech-dashboard/total-company";
  static const String getInfoExhibitionCustomer = "/api/v1/entech-dashboard/total-customer";
  static const String getInfoExhibitionBooth = "/api/v1/entech-dashboard/total-booth";

  static const String getInfoExhibitionCheckIn = "/api/v1/entech-face-datection/find-by-condition?size=100000";

  static const String getFormatImage = "/api/v1/storage";

  static const String printCardVisit = "/api/v1/entech-customer/send-notification-print";

  static const String getInfoExhibitionTotalCheckIn = "/api/v1/entech-dashboard/count-checkin";

  static const String registorAccount = "/api/v1/registration/email";

  // Delete Account
  static const String deleteAccount = "/api/v1/user/cancel-user";

  //----------------------- APPTEACCHER-------------------
  static const String getListShiftOfTeacher = "/api/v1/school-parents/find-by-condition-schedule";

  static const String getListShiftOfParent = "/api/v1/school-parents/find-by-condition-parents";

  static const String getListStudentOfClassShift = "/api/v1/time-keeping/find";
  
  static const String Attendance = "/api/v1/school-parents/attendance-student-by-teacher";
}
