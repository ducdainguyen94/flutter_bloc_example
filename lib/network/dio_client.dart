import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:dio/dio.dart';

import 'package:base_flutter_intlabs/models/response/base_api_response.dart';

final List<String> rejectCode = [
  'jwt expired',
  'un_authorized',
  'invalid-token'
];

//Handle error
errorHandle({DioError? error, bool? base}) {
  String message = "unknown_error";
  Map<String, dynamic>? data;

  switch (error?.type) {
    case DioErrorType.sendTimeout:
    case DioErrorType.receiveTimeout:
      message = "request_time_out";
      break;
    case DioErrorType.response:
      //Expired token
      if(error?.response?.statusCode == 401){
        message = "Tên mật khẩu hoặc tài khoản không chính xác";
        break;
      }
      if (error?.response?.data is Map<String, dynamic>) {
        data = error?.response?.data;
        message = data?['reason'] ?? data?['message'] ?? data?['error_description'] ?? message;
      }
      break;
    default:
      message = "Lỗi kết nối server!";
      break;
  }

  ///Logout
  if (rejectCode.contains(message)) {
    // AppBloc.authBloc.add(OnClear());
  }
  Map<String, dynamic> _result = Map();
  _result["success"] = false;
  _result["message"] = message;
  _result["code"] = error?.response?.statusCode;
  return _result;
}

class DioClient {
  // dio instance
  final Dio _dio;
  // injecting dio instance
  DioClient(this._dio);

  // Get:----------
  Future<dynamic> get(
      String uri, {
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onReceiveProgress,
      }) async {
    try {
      final Response response = await _dio.get(
        uri,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );
      // return response.data;
      return BaseApiResponse(code: response.statusCode, data: response.data, message: response.statusMessage, success: true).toJson();
    } on DioError catch (error) {
      return errorHandle(error: error);
    }
  }

  // Post:----------------------
  Future<dynamic> post(
      String uri, {
        data,
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onSendProgress,
        ProgressCallback? onReceiveProgress,
      }) async {
    try {
      final Response response = await _dio.post(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return BaseApiResponse(code: response.statusCode, data: response.data, message: response.statusMessage, success: true).toJson();
    } on DioError catch (error) {
      return errorHandle(error: error);
    }
}

  // Put:-------------------
  Future<dynamic> put(
      String uri, {
        data,
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onSendProgress,
        ProgressCallback? onReceiveProgress,
      }) async {
    try {
      final Response response = await _dio.put(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return BaseApiResponse(code: response.statusCode, data: response.data, message: response.statusMessage, success: true).toJson();
    } on DioError catch (error) {
      return errorHandle(error: error);
    }
  }

  // Delete:------------------
  Future<dynamic> delete(
      String uri, {
        data,
        Map<String, dynamic>? queryParameters,
        Options? options,
        CancelToken? cancelToken,
        ProgressCallback? onSendProgress,
        ProgressCallback? onReceiveProgress,
      }) async {
    try {
      final Response response = await _dio.delete(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: options,
        cancelToken: cancelToken,
      );
      return BaseApiResponse(code: response.statusCode, data: response.data, message: response.statusMessage, success: true).toJson();
    } on DioError catch (error) {
      return errorHandle(error: error);
    }
  }
}
