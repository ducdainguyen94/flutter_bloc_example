import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:base_flutter_intlabs/database/shared_pref.dart';
import 'package:base_flutter_intlabs/database/shared_preference_helper.dart';
import 'package:base_flutter_intlabs/network/api.dart';
import 'package:base_flutter_intlabs/network/dio_client.dart';
import 'package:base_flutter_intlabs/network/network_module.dart';


GetIt getIt = GetIt.instance;

class Injection {
  static Future<void> inject() async {
    // async singletons:----------------------------------------------------------
    getIt.registerSingletonAsync<SharedPreferences>(
        () => SharedPref.provideSharedPreferences());

    // singletons:----------------------------------------------------------------
    getIt.registerSingleton<SharedPreferenceHelper>(
        SharedPreferenceHelper(await getIt.getAsync<SharedPreferences>()));
    //dio
    getIt.registerSingleton<Dio>(
        NetworkModule.provideDioAuth(getIt<SharedPreferenceHelper>()),
        instanceName: 'DIO_AUTH');
    getIt.registerSingleton<Dio>(
        NetworkModule.provideDio(getIt<SharedPreferenceHelper>()),    // Cái provideDio sẽ là cái quyết định đầu API đầu là gì 
        instanceName: 'DIO_CRM');
    getIt.registerSingleton<Dio>(
        NetworkModule.provideDioProcessor(getIt<SharedPreferenceHelper>()),
        instanceName: 'DIO_PROCESSOR');

    getIt.registerSingleton<DioClient>(
        DioClient(getIt<Dio>(instanceName: 'DIO_AUTH')),
        instanceName: 'DIO_AUTH');
    getIt.registerSingleton<DioClient>(
        DioClient(getIt<Dio>(instanceName: 'DIO_CRM')),
        instanceName: 'DIO_CRM');
    getIt.registerSingleton<DioClient>(
        DioClient(getIt<Dio>(instanceName: 'DIO_PROCESSOR')),
        instanceName: 'DIO_PROCESSOR');

    // api's:---------------------------------------------------------------------
    getIt.registerSingleton(Api(
        getIt.get<DioClient>(instanceName: 'DIO_AUTH'),
        getIt.get<DioClient>(instanceName: 'DIO_CRM'),
        getIt.get<DioClient>(instanceName: 'DIO_PROCESSOR')));
  }     // Đây mới là đoạn khởi tạo ra 1 đối tượng Api mới là lúc truyền tham số constructor cho class Api 
}
