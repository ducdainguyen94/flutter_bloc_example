import 'package:base_flutter_intlabs/app.dart';
import 'package:base_flutter_intlabs/database/shared_preference_helper.dart';
import 'package:base_flutter_intlabs/inject_dependency/injection.dart';
import 'package:base_flutter_intlabs/router/auth_service.dart';
import 'package:base_flutter_intlabs/router/router_name.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:get_it/get_it.dart';

class AuthGuard extends GetMiddleware {

  final authService = Get.find<AuthService>(); //Here come

  @override
  RouteSettings? redirect(String? route) {
    print('GetMiddleware ${authService.isAuth.value}');
    return !authService.isAuth.value
        ? null
        : const RouteSettings(name: RouteName.shiftListScreen);
  }

}