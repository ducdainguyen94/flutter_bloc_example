class RouteName{
  static const initialRoute = '/';
  
  static const String loginScreen = '/LoginScreen';
  static const String shiftListScreen = '/ShiftListScreen';
  static const String detailShiftScreen = '/DetailShiftScreen';

}