import 'package:base_flutter_intlabs/app.dart';
import 'package:base_flutter_intlabs/router/middlewares/auth_guard.dart';
import 'package:base_flutter_intlabs/ui/auth/login/bloc/login_bloc.dart';
import 'package:base_flutter_intlabs/ui/auth/login/login_screen.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/bloc/detail_shift_bloc.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/detail_shift_screen.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/bloc/shift_list_bloc.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/shift_list_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'router_name.dart';

class MyRouter {
  MyRouter._(); //this is to prevent anyone from instantiating this object
  static final getPages = [
    GetPage(
      name: RouteName.loginScreen,
      page: () => BlocProvider(
            create: (context) => LoginBloc(),
            child: LoginScreen(),
          ),
      middlewares: [
        AuthGuard(),
      ]),
    GetPage(
      name: RouteName.shiftListScreen,
      page: () => BlocProvider<ShiftListBloc>(
        lazy: false,
        create: (_) => ShiftListBloc(),
        child: ShiftListScreen(key: listShiftScreenKey,)
      ),
    ),

    GetPage(
      name: RouteName.detailShiftScreen,
      page: () => BlocProvider(
        lazy: false,
        create: (_) => DetailShiftBloc(),
        child: BlocProvider.value(
          value: BlocProvider.of<ShiftListBloc>(listShiftScreenKey.currentContext!),
          child: DetailShiftScreen(key: detailScreenKey,),
        ),
      )  
    ),
  ];
}
