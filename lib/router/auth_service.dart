import 'package:get/get.dart';

class AuthService extends GetxService {
  Future<AuthService> init() async => this;

  final RxBool isAuth = false.obs;

  void setIsAuth(bool newValue) {
    isAuth.value = newValue;
  }
}