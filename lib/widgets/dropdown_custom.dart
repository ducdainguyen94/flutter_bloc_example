import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DropDownCustom extends StatelessWidget {
  List<String> items;
  String? selectedValue;
  dynamic onChange;

  DropDownCustom({required this.items, required this.selectedValue, this.onChange});

  @override
  Widget build(BuildContext context) {
    print('LISTTT+============ ${items.length}');
    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: AppColors.primary),
        borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Center(
          child: DropdownButtonHideUnderline(
            child: DropdownButton2(
              hint: Text(
                'Select Item',
                style: TextStyle(
                  fontSize: 14,
                  color: Theme
                          .of(context)
                          .hintColor,
                ),
              ),
              items: items
                      .map((item) =>
                      DropdownMenuItem<String>(
                        value: item,
                        child: Container(
                          padding: EdgeInsets.only(left: 5, right: 5),
                          child: Text(
                            item,
                            style: const TextStyle(
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ))
                      .toList(),          
              value: selectedValue,
              onChanged: onChange,
              buttonHeight: 40,
              buttonWidth: (MediaQuery.of(context).size.width - 52.w)/2,
              buttonPadding: EdgeInsets.only(left: 10, right: 10),
              itemHeight: 40,
              itemPadding: EdgeInsets.only(left: 0),
              dropdownWidth: (MediaQuery.of(context).size.width - 52.w)/2,
            ),
          ),
        ),
    );
  }
}