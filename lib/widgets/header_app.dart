import 'package:base_flutter_intlabs/common/app_fonts.dart';
import 'package:base_flutter_intlabs/common/app_text_styles.dart';
import 'package:base_flutter_intlabs/ui/shift_list_screen.dart/bloc/shift_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HeaderApp extends StatelessWidget {
  String title;
  HeaderApp({required this.title});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      decoration: BoxDecoration(
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: (){
              Scaffold.of(context).openDrawer();
            },
            child: Container(
              width: 50.w,
              // padding: EdgeInsets.only(left: 16.w, right: 10.w),
              child: Icon(Icons.menu)
            )
          ),
          Spacer(),
          Text(title, textAlign: TextAlign.center, style: AppTextStyle.black.copyWith(fontFamily: AppFonts.bold, fontSize: 16.sp),),
          Spacer(),
            GestureDetector(
                    onTap: (){
                      context.read<ShiftListBloc>().showViewFilter();
                    },
                    child: Container(
                      width: 50,
                      padding: EdgeInsets.only(left: 10.w, right: 10.w),
                      child: Icon(Icons.filter_alt)
                    )
                  ),

        ],
      ),
    );
  }
}