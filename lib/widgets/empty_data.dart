import 'package:base_flutter_intlabs/common/app_fonts.dart';
import 'package:base_flutter_intlabs/common/app_images.dart';
import 'package:base_flutter_intlabs/common/app_text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class EmptyData extends StatelessWidget {
  Widget? image;
  String? title;
  EmptyData({this.title, this.image});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 60.h),
      child: Align(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // SizedBox(
            //   height: 100.h,
            // ),
            image ??
                Image.asset(
                  AppImages.empty,
                  width: 250.h,
                  height: 166.h,
                  fit: BoxFit.fill,
                ),
            SizedBox(
              height: 20.h,
            ),
            Text(
              title ?? 'Không tìm thấy dữ liệu nào',
              textAlign: TextAlign.center,
              style: AppTextStyle.black.copyWith(
                  fontSize: 14.sp,
                  fontFamily: AppFonts.regular,
                  color: Color(0xFFADADAD)),
            ),
            SizedBox(
              height: 8.h,
            ),
          ],
        ),
      ),
    );
  }
}
