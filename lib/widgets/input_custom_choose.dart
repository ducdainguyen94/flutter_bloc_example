import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:base_flutter_intlabs/common/app_text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InputCustomChoose extends StatelessWidget{
  String title;
  int? maxLength;
  TextInputType? textInputType;
  String? hint;
  String? data;
  var onChange;
  var onClick;
  InputCustomChoose(this.title,{this.maxLength,
  this.data,
   this.textInputType, this.onClick, this.hint, this.onChange});
  @override
  Widget build(BuildContext context) {
    final _controller = TextEditingController(text: data);
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(text: TextSpan(
          children: <TextSpan>[
            TextSpan(text: title, style: TextStyle(color: Colors.black, fontSize: 16.sp, fontWeight: FontWeight.w600),),
            TextSpan(text: '*', style: TextStyle(color: Colors.red, fontSize: 16.sp, fontWeight: FontWeight.w600),),
          ]
        )),
        InkWell(
          onTap: (){
            
          },
          child: Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(width: 1, color: AppColors.background))
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: TextField(
                    enabled: false,
                    maxLength: maxLength,
                    onTap: onClick,
                    controller: _controller,
                    onChanged: onChange,
                    keyboardType: textInputType ?? TextInputType.text,
                    decoration: InputDecoration(
                      hintText: hint ?? '',
                      hintStyle: const TextStyle(color: AppColors.background),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                Icon(Icons.keyboard_arrow_down, size: 30,)
              ],
            ),
          ),
        )
      ],
    );
  }
}