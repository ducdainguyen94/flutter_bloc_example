import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DropDownButtonCustom extends StatelessWidget {
  String hint;
  List<dynamic> data;
  dynamic onChangeEvent;

  DropDownButtonCustom({required this.hint, required this.data, required this.onChangeEvent});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 16.w),
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 6.h),
      decoration: BoxDecoration(
          border:
              Border.all(width: 1, color: Colors.blueAccent),
          borderRadius:
              BorderRadius.all(Radius.circular(20.0))),
      child: DropdownButton(
          menuMaxHeight: 300.h,
          underline: SizedBox(),
          value: data,
          icon: Icon(Icons.arrow_drop_down),
          // iconEnabledColor: Colors.red,
          isExpanded: true,
          isDense: true,
          hint: Text(hint),
          items: data.map((item) {
            return DropdownMenuItem(
              value: item,
              child: Text(
                item.nameVi ?? ' ',
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            );
          }).toList(),
          onChanged: onChangeEvent),
    );
  }
}