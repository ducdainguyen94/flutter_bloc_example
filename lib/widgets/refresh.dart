
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class Refresh extends StatelessWidget {
  VoidCallback? onRefresh;
  var controller;
  Widget child;
  bool enablePullDown;
  bool enablePullUp;
  Refresh({ Key? key,
  this.onRefresh,
  this.controller,
  this.enablePullDown = true,
  this.enablePullUp = false,
  required this.child,
   }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      controller: controller,
      enablePullDown: true,
      enablePullUp: false,
      onRefresh: onRefresh,
      header: WaterDropHeader(),
      child: child,
      );
  }
}