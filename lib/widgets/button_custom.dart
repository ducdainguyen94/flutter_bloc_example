import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:base_flutter_intlabs/common/app_text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ButtonCustom extends StatelessWidget {
  final String title;
  final double? width;
  final Color? backgroundColor;
  final TextStyle? styleText;
  final Color? colorText;
  final VoidCallback? onClick;
  final Widget? widget;
  final bool? disable;
  final double? border;
  final bool? showLoading;
  final bool? loading;
  final Color? borderColor;
  const ButtonCustom(
      {Key? key,
      required this.title,
      this.colorText,
      this.borderColor,
      this.backgroundColor,
      this.border,
      this.width,
      this.styleText,
      this.onClick,
      this.widget,
      this.disable,
      this.showLoading,
      this.loading})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(border ?? 12.r),
            color: backgroundColor ?? Colors.blue,
          ),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        onTap: disable == true ? null : onClick,
        splashColor: Colors.black,
        // focusColor: Colors.amber,
        child: Container(
          height: 50.h,
          decoration: BoxDecoration(
            border: Border.all(
                width: 0.5, color: borderColor ?? Colors.transparent),
            borderRadius: BorderRadius.circular(border ?? 12.r),
            // color: AppColors.primary.withOpacity(0.9),
          ),
          width: width ?? MediaQuery.of(context).size.width,
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: 10.w),
          child: loading == false
              ? widget ??
                  Text(
                    title,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: styleText ??
                        AppTextStyle.black
                            .copyWith(color: colorText ?? Colors.white),
                  )
              : SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(
                    color: Colors.white,
                  ),
                ),
        ),
      ),
    );
  }
}
