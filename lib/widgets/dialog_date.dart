import 'package:base_flutter_intlabs/common/app_fonts.dart';
import 'package:base_flutter_intlabs/common/app_text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class DialogDate extends StatefulWidget {
  DateTime initDate;
  var onChange;
  DialogDate({required this.initDate, this.onChange});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DialogDateState();
  }
}

class _DialogDateState extends State<DialogDate>{
  DateTime? _dateTemporatory;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _dateTemporatory = widget.initDate;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
        insetPadding: EdgeInsets.only(right: 16.h, left: 16.h),
        backgroundColor: Colors.transparent,
        child: Container(
          // width: MediaQuery.of(context).size.width - 16,
          height: 300,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            // border: Border.all(width: 1, color: Colors.black),
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          child: Column(
            children: [
              Expanded(
                child: Container(
                  child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (value) {
                      print('DATE $value');
                      setState(() {
                        _dateTemporatory = value;
                      });
                    },
                    initialDateTime: widget.initDate,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Text('Huỷ', style: AppTextStyle.black.copyWith(fontFamily: AppFonts.regular, color: Colors.red),)
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.pop(context);
                      widget.onChange(_dateTemporatory);
                    },
                    child: Text('Chọn', style: AppTextStyle.black.copyWith(fontFamily: AppFonts.regular, color: Colors.white))
                  ),
                ],
              )
            ],
          ),
        ),
      );
  }
}