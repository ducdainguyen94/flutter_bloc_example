
import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:flutter/material.dart';

class CheckboxOption extends StatelessWidget{
  Widget child;
  bool value;
  CheckboxOption({required this.child, required this.value});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      children: [
        Checkbox(  
          checkColor: Colors.red,
          activeColor: Colors.white,  
          value: value,  
          onChanged: (value) {
            
          },
        ),
        Expanded(child: child)
      ],
    );
  }
}