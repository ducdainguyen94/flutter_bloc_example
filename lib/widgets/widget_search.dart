import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WidgetSearch extends StatefulWidget {
  String title;
  dynamic onTextChange;
  WidgetSearch({required this.title, this.onTextChange});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _WidgetSearchState();
  }
}

class _WidgetSearchState extends State<WidgetSearch>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextFormField(
        maxLength: 200,
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
            counterText: "",
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.primary, width: 1.h),
              borderRadius: BorderRadius.circular(10.h),
            ),
            enabledBorder:
                OutlineInputBorder(borderSide: BorderSide(color: AppColors.primary, width: 1), borderRadius: BorderRadius.circular(10.0)),
            hintText: widget.title,
            // hintStyle: TextStyle(fontSize: 16.sp),
            isDense: true,
            contentPadding: EdgeInsets.symmetric(vertical: 11.0.h, horizontal: 15.0.w)
            ),
        onChanged: widget.onTextChange,
    );
  }
}