import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:base_flutter_intlabs/common/app_text_styles.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/bloc/detail_shift_bloc.dart';
import 'package:base_flutter_intlabs/ui/detail_shift_screen.dart/bloc/detail_shift_state.dart';
import 'package:base_flutter_intlabs/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:tuple/tuple.dart';
import 'package:intl/intl.dart';

DateFormat dateFormat = DateFormat('yyyy-MM-dd');

class InputChooseDate extends StatefulWidget {
  dynamic onChangeDay;
  String? startTime;
  String? endTime;
  // DateFormat? f = DateFormat("dd-MM");

  InputChooseDate({Key? key, this.onChangeDay, this.startTime, this.endTime}) : super(key: key);

  @override
  State<InputChooseDate> createState() => _InputChooseDateState();
}

class _InputChooseDateState extends State<InputChooseDate> {
//part show dialog calendar
  _showDialogCalendar() {
    showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(12.r), topRight: Radius.circular(12.r)),
        ),
        context: context,
        isScrollControlled: true,
        builder: (_) {
          String? startDay;
          String? endDay;

          /// The method for [DateRangePickerSelectionChanged] callback, which will be
          /// called whenever a selection changed on the date picker widget.
          void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
            print('DATATA RANGE======== ${args.value}');
            if (args.value is PickerDateRange) {
              var _range = '${DateFormat('yyyy-MM-dd').format(args.value.startDate)} -'
                  ' ${DateFormat('dd/MM/yyyy').format(args.value.endDate ?? args.value.startDate)}';

              startDay = DateFormat('yyyy-MM-dd').format(args.value.startDate);
              endDay = DateFormat('yyyy-MM-dd').format(args.value.endDate ?? args.value.startDate);

              print('DATATA RANGE ${args.value}');

              // onChangeDay(startDay, endDay);
            } else if (args.value is DateTime) {
              var _selectedDate = args.value.toString();

              startDay = DateFormat('yyyy-MM-dd').format(args.value);
              endDay = DateFormat('YYYY-MM-DD').format(args.value);

              // onChangeDay(startDay, endDay);
              print('DATATA_DATETIME $_selectedDate');
            } else if (args.value is List<DateTime>) {
              var _dateCount = args.value.length.toString();
              print('DATATA_LÍT $_dateCount');
            } else {
              var _rangeCount = args.value.length.toString();
              print('DATATA_ELS $_rangeCount');
            }
          }

          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  decoration: const BoxDecoration(color: AppColors.primary, border: Border(bottom: BorderSide(width: 0.5, color: Colors.grey))),
                  child: Row(
                    children: [
                      Button(
                          label: 'Huỷ',
                          type: 'text',
                          colorText: Colors.white,
                          onClick: () {
                            Navigator.pop(context);
                          }),
                      Expanded(
                          child: Container(
                        height: 40,
                        child: Center(
                          child: Text(
                            'Chọn khoảng thời gian',
                            textAlign: TextAlign.center,
                            style: AppTextStyle.black.copyWith(color: Colors.white),
                          ),
                        ),
                      )),
                      Button(
                          label: 'Xong',
                          type: 'text',
                          colorText: Colors.white,
                          onClick: () {
                            Navigator.pop(context);
                            widget.onChangeDay(startDay!, endDay!);
                          }),
                    ],
                  ),
                ),
                SfDateRangePicker(
                  onSelectionChanged: _onSelectionChanged,

                  selectionMode: DateRangePickerSelectionMode.range,
                  // minDate: DateTime.now().subtract(const Duration(days: 7)),
                  // maxDate: DateTime.now(),
                  initialSelectedRange: PickerDateRange(dateFormat.parse(widget.startTime!), dateFormat.parse(widget.endTime!)),
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 42,
        padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 11.h),
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: AppColors.primary),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color: Colors.white,
        ),
        child: InkWell(
          onTap: () {
            _showDialogCalendar();
            final datetmp = DateTime.parse(widget.startTime!);
            print(datetmp);
          },
          child: widget.startTime == null || widget.endTime == null
              ? Text(
                  'Chọn ngày',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black.withOpacity(0.65)),
                )
              :
              //  Text('${ DateFormat('dd-MM').format(DateFormat('dd-MM-yyyy').parse(widget.startTime!))} - ${ DateFormat('dd-MM').format(DateFormat('dd-MM-yyyy').parse(widget.endTime!))}',
              Text('${DateFormat('dd/MM').format(DateTime.parse(widget.startTime!))} - ${DateFormat('dd/MM').format(DateTime.parse(widget.endTime!))}',
              // Text('${DateTime.parse(widget.startTime!).day}-${DateTime.parse(widget.startTime!).month} / ${DateTime.parse(widget.startTime!).day}-${DateTime.parse(widget.startTime!).month}',

                  // : Text('${widget.startTime}',
                  textAlign: TextAlign.center,
                ),
        ));
  }
}
