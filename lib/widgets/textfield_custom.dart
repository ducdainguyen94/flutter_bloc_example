import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class InputCustom extends StatelessWidget{
  String title;
  int? maxLength;
  TextInputType? textInputType;
  String? data;
  String? hint;
  var onChange;
 
  InputCustom(this.title,{this.maxLength,this.data, this.textInputType, this.hint, this.onChange});

  // final _controller = 
  @override
  Widget build(BuildContext context) {
     final _controller = TextEditingController(text: data);
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(text: TextSpan(
          children: <TextSpan>[
            TextSpan(text: title, style: TextStyle(color: Colors.black, fontSize: 16.sp, fontWeight: FontWeight.w600),),
            TextSpan(text: '*', style: TextStyle(color: Colors.red, fontSize: 16.sp, fontWeight: FontWeight.w600),),
          ]
        )),
        TextField(
          maxLength: maxLength,
          onChanged: onChange,
          controller: _controller,
          keyboardType: textInputType ?? TextInputType.text,
          decoration: InputDecoration(
            hintText: hint ?? '',
            hintStyle: const TextStyle(color: AppColors.background)
          ),
        )
      ],
    );
  }
}