import 'package:base_flutter_intlabs/database/shared_preference_helper.dart';
import 'package:base_flutter_intlabs/inject_dependency/injection.dart';
import 'package:base_flutter_intlabs/router/auth_service.dart';
import 'package:base_flutter_intlabs/router/router_name.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NavDrawer extends StatelessWidget {


  //click logout
  clickLogout() async {
    await getIt.get<SharedPreferenceHelper>().removeAuthToken(); // xoá authToken ra khỏi SharePreference
    await getIt.get<SharedPreferenceHelper>().removeOrganizationId(); // xoá id tổ chức khỏi sharePreference
    //reset bloc
    Get.find<AuthService>().setIsAuth(false);
    Get.offAllNamed(RouteName.loginScreen);

  }


  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          // DrawerHeader(
          //   child: Text(
          //     'Side menu',
          //     style: TextStyle(color: Colors.white, fontSize: 25),
          //   ),
          //   decoration: BoxDecoration(
          //       color: Colors.green,
          //       image: DecorationImage(
          //           fit: BoxFit.fill,
          //           image: AssetImage('assets/images/img_exhibition.jpg'))),
          // ),
          ListTile(
            leading: Icon(Icons.verified_user),
            title: Text('Thông tin'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Cài đặt'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Đăng xuất'),
            onTap: clickLogout,
          ),
        ],
      ),
    );
  }
}