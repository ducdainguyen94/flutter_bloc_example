import 'package:base_flutter_intlabs/common/app_colors.dart';
import 'package:flutter/material.dart';

final data = List.generate(10, (index) => User(name: 'name $index', imgUrl: 'imgUrl'));

class AutoCompleteCheckIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 40,
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: AppColors.primary),
        borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Autocomplete<User>(
        optionsBuilder: (textEditingValue){
          if(textEditingValue.text.isEmpty){
            return data;
          }else {
            return data.where((element) => element.name.toLowerCase().contains(textEditingValue.text.toLowerCase())).toList();
          }
        },
        fieldViewBuilder: (context, textEditingController, focusNode, onFieldSubmitted){
          return TextField(
            controller: textEditingController,
            focusNode: focusNode,
            onEditingComplete: onFieldSubmitted,
            decoration: const InputDecoration(
              hintText: 'Search data ',
              border: InputBorder.none,
            ),     
          );
        },
        optionsViewBuilder: (context, onSelected, users){
          return SafeArea(
            left: false,
            right: false,
            child: Scaffold(
              body: Container(
                // color: Colors.red,
                // width: 200,
                child: ListView.separated(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  itemBuilder: ((BuildContext context, int index){
                    final user = data[index];
                    return GestureDetector(
                      onTap: (){
                        print('VALUE SELECTED======  $user');
                        onSelected(user);
                      },
                      child: Container(
                        // color: user.name == 'name 2' ? Colors.black : Colors.white,
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: Text(user.name + 'HHHHH'),
                      ),
                    );
                  }), 
                  separatorBuilder: (BuildContext context, int index){
                    return Container();
                  }, 
                  itemCount: users.length
                ),
              ),
            ),
          );
        },
        onSelected: (user){
          print('VALUE SELECTED  $user');
        },
        displayStringForOption: ((user) => user.name),
      ),
    );
  }
}





class User {
  String name;
  String imgUrl;

  User({required this.name, required this.imgUrl});
}